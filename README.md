# hsc3-data

Data functions for
[SC3](http://audiosynth.com/)
related work in
[hsc3](http://rohandrape.net/?t=hsc3)
([haskell](http://haskell.org/) supercollider).

## cli

[ats](?t=hsc3-data&e=md/ats.md),
[au-to-pbm](?t=hsc3-data&e=md/au-to-pbm.md),
[au-to-pgm](?t=hsc3-data&e=md/au-to-pgm.md),
[csv-mnd-to-pgm](?t=hsc3-data&e=md/csv-mnd-to-pgm.md),
[img-edit](?t=hsc3-data&e=md/img-edit.md),
[img-to-pbm](?t=hsc3-data&e=md/img-to-pbm.md),
[img-to-pgm](?t=hsc3-data&e=md/img-to-pgm.md),
[img-to-sf](?t=hsc3-data&e=md/img-to-sf.md),
[midi](?t=hsc3-data&e=md/midi.md),
[pbm-indices](?t=hsc3-data&e=md/pbm-indices.md),
[pbm-to-csv-mnd](?t=hsc3-data&e=md/pbm-to-csv-mnd.md),
[pbm-to-tbl](?t=hsc3-data&e=md/pbm-to-tbl.md),
[pbm-trace](?t=hsc3-data&e=md/pbm-trace.md),
[pdb](?t=hsc3-data&e=md/pdb.md),
[pgm-to-au](?t=hsc3-data&e=md/pgm-to-au.md),
[sf-condense](?t=hsc3-data&e=md/sf-condense.md),
[sf-draw](?t=hsc3-data&e=md/sf-draw.md),
[sf-to-png](?t=hsc3-data&e=md/sf-to-png.md),
[text-monitor](?t=hsc3-data&e=md/text-monitor.md),
[trace-rec-x11](?t=hsc3-data&e=md/trace-rec-x11.md),
[x11-ptr-to-midi-osc](?t=hsc3-data&e=md/x11-ptr-to-midi-osc.md)

© [rohan drape](http://rohandrape.net/),
  2013-2025,
  [gpl](http://gnu.org/copyleft/)

* * *

```
$ make doctest
Examples: 293  Tried: 293  Errors: 0  Failures: 0
$
```
