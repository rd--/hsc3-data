import Control.Exception {- base -}
import Control.Monad {- base -}
import Data.Bits {- base -}
import System.Environment {- base -}
import System.Exit {- base -}
import Text.Printf {- base -}

import qualified Graphics.X11.Xlib as X {- xlib -}

import Sound.Osc.Fd {- hosc -}
import qualified Sound.Osc.Transport.Fd.Udp as Fd.Udp {- hosc -}

import Sound.Sc3.Data.X11.Ptr {- hsc3-data -}

{- midi-osc -}
import qualified Sound.Midi.Osc as M {- midi-osc -}
import qualified Sound.Midi.Type as M

-- * Ptr Midi-osc

to_cc_msg :: Int -> Int -> Double -> Message
to_cc_msg ch cc r = M.cvm_to_osc 0xFF (M.Control_Change ch cc (floor (r * 127)))

-- > ptr_midi_osc (0.01,0,1,0,2)
ptr_midi_osc :: (Double, Int, Int, Int, Int) -> IO ()
ptr_midi_osc (dt, x_ch, x_cc, y_ch, y_cc) = do
  x11 <- x11_init ":0"
  fd <- Fd.Udp.openUdp "127.0.0.1" 57150 -- midi-osc
  let recur (rx', ry') = do
        ((rx, ry), _, (x, y), _) <- x11_ptr_raw x11
        when (rx /= rx') (sendMessage fd (to_cc_msg x_ch x_cc x))
        when (ry /= ry') (sendMessage fd (to_cc_msg y_ch y_cc y))
        pauseThread dt
        recur (rx, ry)
  finally (recur (0, 0)) (x11_close x11 >> close fd)

-- * Ptr Trace

entry :: Double -> Double -> Double -> String
entry tm x y = printf "%.3f,%.3f,%.3f" tm x y

has_mask :: Bits a => a -> a -> Bool
has_mask k m = (m .&. k) == k

-- > ptr_trace False 0.01
ptr_trace :: Bool -> Double -> IO ()
ptr_trace u dt = do
  putStrLn "tm,x,y"
  x11 <- x11_init ":0"
  t0 <- time
  let recur (rx', ry') = do
        tm <- time
        ((rx, ry), c1, c2, m) <- x11_ptr_raw x11
        let (x, y) = if u then c1 else c2
        when (has_mask X.button3Mask m) exitSuccess
        when (rx /= rx' || ry /= ry') (putStrLn (entry (tm - t0) x y))
        pauseThread dt
        recur (rx, ry)
  finally (recur (0, 0)) (x11_close x11)

-- * Main

help :: [String]
help =
  [ "hsc3-x11"
  , ""
  , "  ptr midi-osc dt:float x-ch:int x-cc:int y-ch:int y-cc:int"
  , "  ptr trace u|n delta-time:float"
  ]

main :: IO ()
main = do
  a <- getArgs
  case a of
    ["ptr", "midi-osc", dt, x_ch, x_cc, y_ch, y_cc] ->
      ptr_midi_osc (read dt, read x_ch, read x_cc, read y_ch, read y_cc)
    ["ptr", "trace", nrm, dt] -> ptr_trace (nrm == "u") (read dt)
    _ -> putStrLn (unlines help)
