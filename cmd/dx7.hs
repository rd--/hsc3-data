import Control.Monad {- base -}
import Data.List {- base -}
import System.IO {- base -}
import Text.Printf {- base -}

import Data.List.Split {- split -}

import qualified Music.Theory.Array.Csv as Array.Csv {- hmt-base -}
import qualified Music.Theory.Byte as Byte {- hmt-base -}
import qualified Music.Theory.Opt as Opt {- hmt-base -}

import qualified Sound.Sc3.Data.Yamaha.Dx7 as Dx7 {- hsc3-data -}
import qualified Sound.Sc3.Data.Yamaha.Dx7.Db as Dx7.Db {- hsc3-data -}
import qualified Sound.Sc3.Data.Yamaha.Dx7.Hash as Dx7.Hash {- hsc3-data -}
import qualified Sound.Sc3.Data.Yamaha.Dx7.Pp as Dx7.Pp {- hsc3-data -}

usage_str :: [String]
usage_str =
  [ "hsc3-dx7 cmd opt"
  , "  {hex | sysex} print all|ix print-cmd file-name..."
  , "  sysex add input-file output-file"
  , "  sysex rewrite input-file output-file"
  , "  sysex verify file-name..."
  , ""
  , "    print-cmd = concise | csv | hash-hex | hash-names | hex | names | parameters | voice-data-list"
  , "    ix = list-of-int (one-indexed)"
  ]

usage :: IO ()
usage = putStrLn (unlines usage_str)

type Load_F = FilePath -> IO (Maybe [Dx7.Dx7_Voice])

type Sel = Maybe [Int]

dx7_print_f :: Sel -> Load_F -> ((Int, Dx7.Dx7_Voice) -> String) -> [FilePath] -> IO ()
dx7_print_f sel ld_f op =
  let sel_f x = maybe x (\r -> filter (\(k, _) -> k `elem` r) x) sel
      wr_f fn x = case x of
        Just bnk -> putStr (unlines (map op (sel_f (zip [1 :: Int ..] bnk))))
        Nothing -> hPutStrLn stderr ("ERROR: dx7_sysex_print: " ++ fn)
  in mapM_ (\fn -> ld_f fn >>= wr_f fn)

-- > let fn = "/home/rohan/sw/hsc3-data/data/yamaha/dx7/vrc/VRC-106-B.syx"
-- > dx7_sysex_print (Just [1]) "concise" [fn]
-- > dx7_sysex_print (Just [1,32]) "names" [fn]
-- > dx7_sysex_print (Just [2,31]) "hash-names" [fn]
dx7_print :: Bool -> Sel -> Load_F -> String -> [FilePath] -> IO ()
dx7_print leadingZeroes sel ld cmd fn =
  let print_concise = unlines . Dx7.Pp.dx7_voice_concise_str . snd
      print_csv = Dx7.Pp.dx7_voice_to_csv . snd
      print_hex pr_h (_, v) =
        if pr_h
          then intercalate "," (Dx7.Db.dx7_hash_vc_param_csv (Dx7.Db.dx7_hash_vc v))
          else Byte.byte_seq_hex_pp False v
      print_parameters = unlines . Dx7.Pp.dx7_parameter_seq_pp (leadingZeroes, True) . snd
      print_voice_data_list = unlines . Dx7.Pp.dx7_voice_data_list_pp leadingZeroes . snd
      print_voice_name pr_h (k, v) =
        let nm = Dx7.dx7_voice_name '?' v
        in if pr_h
            then
              let h = Dx7.Hash.dx7_voice_hash v
              in printf "%s,%s" (Dx7.Hash.dx7_hash_pp h) (Array.Csv.csv_quote_if_req nm)
            else printf "%2d %s" k nm
      print_f f = dx7_print_f sel ld f fn
  in case cmd of
      "concise" -> print_f print_concise
      "csv" -> print_f print_csv
      "hash-hex" -> print_f (print_hex True)
      "hash-names" -> print_f (print_voice_name True)
      "hex" -> print_f (print_hex False)
      "names" -> print_f (print_voice_name False)
      "parameters" -> print_f print_parameters
      "voice-data-list" -> print_f print_voice_data_list
      _ -> usage

dx7_hex_print :: Bool -> Sel -> String -> [FilePath] -> IO ()
dx7_hex_print opt sel = dx7_print opt sel (fmap Just . Dx7.dx7_load_hex)

dx7_sysex_print :: Bool -> Sel -> String -> [FilePath] -> IO ()
dx7_sysex_print opt sel = dx7_print opt sel Dx7.dx7_load_sysex_try

dx7_sysex_verify_1 :: FilePath -> IO ()
dx7_sysex_verify_1 fn = do
  dat <- Dx7.dx7_read_fmt9_sysex_err fn
  bnk <- Dx7.dx7_load_fmt9_sysex_err fn
  let r = Dx7.dx7_fmt9_sysex_verify 0 dat == (True, True, True, True) && Dx7.dx7_bank_verify True bnk
  putStrLn (if r then "TRUE" else "FALSE: " ++ show (dat, bnk))

dx7_sysex_verify :: [FilePath] -> IO ()
dx7_sysex_verify = mapM_ dx7_sysex_verify_1

dx7_sysex_add :: FilePath -> FilePath -> IO ()
dx7_sysex_add fn1 fn2 = do
  dat <- Dx7.dx7_read_u8 fn1
  when (length dat /= 4096) (error "dx7_sysex_add: NOT 4096")
  Dx7.dx7_write_fmt9_sysex fn2 (Dx7.dx7_fmt9_sysex_gen 0 dat)

dx7_sysex_rewrite :: FilePath -> FilePath -> IO ()
dx7_sysex_rewrite fn1 fn2 = do
  src <- Dx7.dx7_read_u8 fn1 -- ie. do not verify
  let dat = Dx7.dx7_fmt9_sysex_dat src
  when (length dat /= 4096) (error "dx7_sysex_rewrite?")
  Dx7.dx7_write_fmt9_sysex fn2 (Dx7.dx7_fmt9_sysex_gen 0 dat)

main :: IO ()
main = do
  let opt_def = [("leadingZeroes", "False", "bool", "Print leading zeroes")]
  (o, a) <- Opt.opt_get_arg True usage_str opt_def
  let lz = Opt.opt_read o "leadingZeroes"
      sel_f x = if x == "all" then Nothing else Just (map read (splitOn "," x))
  case a of
    "hex" : "print" : sel : cmd : fn -> dx7_hex_print lz (sel_f sel) cmd fn
    ["sysex", "add", fn1, fn2] -> dx7_sysex_add fn1 fn2
    "sysex" : "print" : sel : cmd : fn -> dx7_sysex_print lz (sel_f sel) cmd fn
    ["sysex", "rewrite", fn1, fn2] -> dx7_sysex_rewrite fn1 fn2
    "sysex" : "verify" : fn -> dx7_sysex_verify fn
    _ -> usage
