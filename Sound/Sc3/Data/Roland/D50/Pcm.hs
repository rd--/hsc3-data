-- | D50 / Pcm
module Sound.Sc3.Data.Roland.D50.Pcm where

import Sound.Sc3.Data.Math.Types {- hsc3-data -}

-- | (Wg-Pcm-Wave-Number,Indication:Pcm-Name)
d50_pcm_wave_tbl :: [(U8, String)]
d50_pcm_wave_tbl =
  [ (001, "Marmba:Marimba")
  , (002, "Vibes :Vibraphone")
  , (003, "Xylo1 :Xylophone 1")
  , (004, "Xylo2 :Xylophone 2")
  , (005, "Log_Bs:Log bass")
  , (006, "Hammer:Hammer")
  , (007, "JpnDrm:Japanese Drum")
  , (008, "Kalmba:Kalimba")
  , (009, "Pluck :Pluck 1")
  , (010, "Chink :Chink")
  , (011, "Agogo :Agogo")
  , (012, "3angle:Triangle")
  , (013, "Bells :Bells")
  , (014, "Nails :Nail File")
  , (015, "Pick  :Pick")
  , (016, "Lpiano:Low Piano")
  , (017, "Mpiano:Mid Piano")
  , (018, "Hpiano:High Piano")
  , (019, "Harpsi:Harpsichord")
  , (020, "Harp  :Harp")
  , (021, "Orgprc:Organ Percussion")
  , (022, "Steel :Steel Strings")
  , (023, "Nylon :Nylon Strings")
  , (024, "Eguit1:Electric Guitar 1")
  , (025, "Eguit2:Electric Guitar 2")
  , (026, "Dirt  :Dirty Guitar")
  , (027, "P_Bass:Pick Bass")
  , (028, "Pop   :Pop Bass")
  , (029, "Thump :Thump")
  , (030, "Uprite:Upright Bass")
  , (031, "Clarnt:Clarinet")
  , (032, "Breath:Breath")
  , (033, "Steam :Steamer")
  , (034, "FluteH:High Flute")
  , (035, "FluteL:Low Flute")
  , (036, "Guiro :Guiro")
  , (037, "IndFlt:Indian Flute")
  , (038, "Harmo :Flute Harmonics")
  , (039, "Lips1 :Lips 1")
  , (040, "Lips2 :Lips 2")
  , (041, "Trumpt:Trumpet")
  , (042, "Bones :Trombones")
  , (043, "Contra:Contrabass")
  , (044, "Cello :Cello")
  , (045, "VioBow:Violin bow")
  , (046, "Violns:Violins")
  , (047, "Pizz  :Pizzicart")
  , (048, "Drawbr:Draw bars (Loop)")
  , (049, "Horgan:High Organ (Loop)")
  , (050, "Lorgan:Low Organ (Loop)")
  , (051, "EP_lp1:Electric Piano (Loop 1)")
  , (052, "EP_lp2:Electric Piano (Loop 2)")
  , (053, "CLAVlp:Clavi (Loop)")
  , (054, "HC_lp :Harpsichord (Loop)")
  , (055, "EP_lp1:Electric Bass (Loop 1)")
  , (056, "AB_lp :Acoustic Bass (Loop)")
  , (057, "EB_lp2:Electric Bass (Loop 2)")
  , (058, "EB_lp3:Electric Bass (Loop 3)")
  , (059, "EG_lp :Electric Guitar (Loop)")
  , (060, "CELLlp:CELLlp (Loop)")
  , (061, "VIOLlp:Violin (Loop)")
  , (062, "Reedlp:Lead (Loop)")
  , (063, "SAXip1:Sax (Loop 1)")
  , (064, "SAXlp2:Sax (Loop 2)")
  , (065, "Aah_lp:Aah (Loop)")
  , (066, "Ooh_lp:Ooh (Loop)")
  , (067, "Manlp1:Male (Loop 1)")
  , (068, "Spect1:Spectrum 1 (Loop)")
  , (069, "Spect2:Spectrum 2 (Loop)")
  , (070, "Spect3:Spectrum 3 (Loop)")
  , (071, "Spect4:Spectrum 4 (Loop)")
  , (072, "Spect5:Spectrum 5 (Loop)")
  , (073, "Spect6:Spectrum 6 (Loop)")
  , (074, "Spect7:Spectrum 7 (Loop)")
  , (075, "Manlp2:Male (Loop 2)")
  , (076, "Noise :Noise (Loop)")
  , (077, "Loop01")
  , (078, "Loop02")
  , (079, "Loop03")
  , (080, "Loop04")
  , (081, "Loop05")
  , (082, "Loop06")
  , (083, "Loop07")
  , (084, "Loop08")
  , (085, "Loop09")
  , (086, "Loop10")
  , (087, "Loop11")
  , (088, "Loop12")
  , (089, "Loop13")
  , (090, "Loop14")
  , (091, "Loop15")
  , (092, "Loop16")
  , (093, "Loop17")
  , (094, "Loop18")
  , (095, "Loop19")
  , (096, "Loop20")
  , (097, "Loop21")
  , (098, "Loop22")
  , (099, "Loop23")
  , (100, "Loop24")
  ]

-- | Expanded to have Indication and Pcm-Name as separate strings.
d50_pcm_wave_tbl_exp :: [(U8, String, String)]
d50_pcm_wave_tbl_exp =
  let f (n, txt) = (n, take 6 txt, drop 7 txt)
  in map f d50_pcm_wave_tbl
