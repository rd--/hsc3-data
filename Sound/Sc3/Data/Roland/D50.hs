{- | Roland D-50

Roland Corporation.
/Roland MIDI Linear Synthesiser Programmer PG-1000 Owner's Manual/.
Hamamatsu, JP, 1987.
<http://cdn.roland.com/assets/media/pdf/PG-1000_OM.pdf>

Roland Corporation.
/D-50 Service Notes/.
Hamamatsu, JP, 1987.
<http://www.synfo.nl/servicemanuals/Roland/D-50_SERVICE_NOTES.pdf>
-}
module Sound.Sc3.Data.Roland.D50 where

import Control.Monad {- base -}
import Data.Char {- base -}
import Data.Function {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}

import qualified Data.List.Split as Split {- split -}

import qualified Music.Theory.Byte as Byte {- hmt-base -}
import qualified Music.Theory.List as List {- hmt-base -}
import qualified Music.Theory.Maybe as Maybe {- hmt-base -}

import qualified Sound.Midi.Common as Midi {- midi-osc -}
import qualified Sound.Midi.Constant as Constant {- midi-osc -}

import Sound.Sc3.Data.Math.Types {- hsc3-data -}

import Sound.Sc3.Data.Roland.D50.Pcm {- hsc3-data -}

-- * Roland

{- | The checksum is a derived from the address (three bytes)
and the data bytes.

<ftp://ftp.monash.edu.au/pub/midi/DOC/Roland-checksum>

> roland_checksum [0x40,0x00,0x04,0x64] == 0x58
-}
roland_checksum :: [U8] -> U8
roland_checksum =
  let f n w =
        case w of
          [] -> 0x80 - n
          x : w' -> let n' = n + x in f (if n' > 0x80 then n' - 0x80 else n') w'
  in f 0

-- * Parameter Type

-- | A patch has two tones, 'Upper' and 'Lower'.
data Tone = Upper | Lower deriving (Eq, Show)

-- | A 'Tone' has two partials, 'One' and 'Two'.
data Partial_Ix = One | Two deriving (Eq, Show)

{- | Parameters are of one of seven types.
  Four 'Partial's (U1,U2,L1,L2), two 'Common' (U,L), or 'Patch'.
-}
data D50_Parameter_Type = Partial Tone Partial_Ix | Common Tone | Patch deriving (Eq, Show)

-- * Parameter

-- | Either a numeric range or a semi-colon delimited sequence of strings.
type D50_Usr_Str = String

-- | Given USR /str/ lookup value at index /x/, or /def/ if non-USR string.
d50_usr_ix :: String -> D50_Usr_Str -> U8 -> String
d50_usr_ix def usr_str x =
  case Split.splitOn ";" usr_str of
    [_] -> def
    e -> u8_at e x

{- | Lookup index of USR string.

> d50_usr_lookup d50_partial_mute_usr "01" == Just 2
-}
d50_usr_lookup :: D50_Usr_Str -> String -> Maybe U8
d50_usr_lookup usr_str nm = fmap int_to_u8_err (elemIndex nm (Split.splitOn ";" usr_str))

-- | Erroring variant.
d50_usr_lookup_err :: D50_Usr_Str -> String -> U8
d50_usr_lookup_err usr_str = fromMaybe (error "d50_usr_lookup?") . d50_usr_lookup usr_str

-- | (INDEX:U24,NAME,STEPS:U8,USR-OFFSET:I8,USR-STRING)
type D50_Parameter = (U24, String, U8, I8, D50_Usr_Str)

d50_parameter_ix :: D50_Parameter -> U24
d50_parameter_ix (ix, _, _, _, _) = ix

d50_parameter_name :: D50_Parameter -> String
d50_parameter_name (_, nm, _, _, _) = nm

d50_parameter_user_offset :: D50_Parameter -> I8
d50_parameter_user_offset (_, _, _, o, _) = o

-- * Patch Bank

{- | Translate one-indexed (BANK-NUMBER,PATCH-NUMBER) index to zero linear index.

> map d50_bank_to_ix [(1,1),(2,3),(3,1),(4,7),(7,4),(8,8)] == [0,10,16,30,51,63]
-}
d50_bank_to_ix :: Num n => (n, n) -> n
d50_bank_to_ix (p, q) = ((p - 1) * 8) + (q - 1)

{- | Inverse of 'd50_bank_to_ix'.

> map d50_ix_to_bank [0,10,51,63] == [(1,1),(2,3),(7,4),(8,8)]
-}
d50_ix_to_bank :: Integral i => i -> (i, i)
d50_ix_to_bank n = let (p, q) = n `divMod` 8 in (p + 1, q + 1)

-- * Address

-- | Parameter address.
type D50_Address = U24

{- | Parse the address notation used in Roland manuals.

> d50_addr_parse_t3 "00-02-40" == Just (0x00,0x02,0x40)
-}
d50_addr_parse_t3 :: String -> Maybe (U8, U8, U8)
d50_addr_parse_t3 str =
  case Byte.read_hex_byte_seq (filter (/= '-') str) of
    [d1, d2, d3] -> Just (d1, d2, d3)
    _ -> Nothing

{- | Encode D50 address, alias for 'Midi.bits_21_join_be'

> d50_addr_encode (0x02,0x00,0x00) == 0x8000
-}
d50_addr_encode :: (U8, U8, U8) -> D50_Address
d50_addr_encode = Midi.bits_21_join_be

d50_addr_decode :: D50_Address -> (U8, U8, U8)
d50_addr_decode = Midi.bits_21_sep_be

-- | 'd50_addr_encode' of 'd50_addr_parse_t3'
d50_addr_read :: String -> D50_Address
d50_addr_read = d50_addr_encode . fromMaybe (error "d50_addr_read?") . d50_addr_parse_t3

{- | Base address (offset) for each parameter type (as 7-bit 3-tuple).

4.1 Parameter base address

[00-00-00] Upper Partial 1 (  0 -  53)
[00-00-40] Upper Partial 2 ( 64 - 117)
[00-01-00] Upper Common    (128 - 175)
[00-01-40] Lower Partial 1 (192 - 245)
[00-02-00] Lower Partial 2 (256 - 309)
[00-02-40] Lower Common    (320 - 367)
[00-03-00] Patch           (384 - 420)

> map (d50_addr_encode . d50_parameter_type_base_address_t3) parameter_type_univ
-}
d50_parameter_type_base_address_t3 :: D50_Parameter_Type -> (U8, U8, U8)
d50_parameter_type_base_address_t3 ty =
  case ty of
    Partial Upper One -> (0x00, 0x00, 0x00) -- 0x000 = 000
    Partial Upper Two -> (0x00, 0x00, 0x40) -- 0x040 = 064
    Common Upper -> (0x00, 0x01, 0x00) -- 0x080 = 128
    Partial Lower One -> (0x00, 0x01, 0x40) -- 0x0C0 = 192
    Partial Lower Two -> (0x00, 0x02, 0x00) -- 0x100 = 256
    Common Lower -> (0x00, 0x02, 0x40) -- 0x140 = 320
    Patch -> (0x00, 0x03, 0x00) -- 0x180 = 384

{- | Base address (offset) for each parameter type (as 'D50_Address').
     These are also the actual addresses of the "temporary area".

> map parameter_type_base_address parameter_type_seq == [0,64,128,192,256,320,384]
> [0,64,128,192,256,320,384] == [0x000,0x040,0x080,0x0C0,0x100,0x140,0x180]
-}
d50_parameter_type_base_address :: D50_Parameter_Type -> D50_Address
d50_parameter_type_base_address = d50_addr_encode . d50_parameter_type_base_address_t3

-- | List of name address segments.
d50_name_address_segments :: [(D50_Address, U24)]
d50_name_address_segments = [(0x080, 10), (0x140, 10), (0x180, 18)]

-- | Is /addr/ in any of the D50 name address segments?
d50_is_name_address :: D50_Address -> Bool
d50_is_name_address n = let f (p, q) = n >= p && n < (p + q) in any f d50_name_address_segments

-- | Base-address for partials U1, U2, L1 and L2.
d50_partial_base_address_seq :: [D50_Address]
d50_partial_base_address_seq = [0x000, 0x040, 0x0C0, 0x100]

{- | Base address, initial offset (ie. past tone or patch name), number of (non-name) parameters.

> sum (map (\ty -> let (_,_,n) = d50_parameter_type_extent ty in n) d50_parameter_type_seq) == 314
> let (b,o,n) = d50_parameter_type_extent (last d50_parameter_type_seq) in b + o + n == 424
-}
d50_parameter_type_extent :: D50_Parameter_Type -> (D50_Address, U24, U24)
d50_parameter_type_extent ty =
  case ty of
    Partial _ _ -> (d50_parameter_type_base_address ty, 0, d50_partial_parameters_n)
    Common _ -> (d50_parameter_type_base_address ty, 10, d50_common_factors_n) -- TONE NAME = 10-CHAR
    Patch -> (d50_parameter_type_base_address ty, 18, d50_patch_factors_n) -- PATCH NAME = 18-CHAR

-- | All 'D50_Parameter_Type' in sequence, ie. in ascending ADDRESS order).
d50_parameter_type_seq :: [D50_Parameter_Type]
d50_parameter_type_seq =
  [ Partial Upper One
  , Partial Upper Two
  , Common Upper
  , Partial Lower One
  , Partial Lower Two
  , Common Lower
  , Patch
  ]

{- | Parameter base address (Top address) (4.1), (TYPE,(START-ADDR,END-ADDR))
  Segments INCLUDE the three NAME data segments.
-}
d50_parameter_type_address_segments :: [(D50_Parameter_Type, (D50_Address, D50_Address))]
d50_parameter_type_address_segments =
  let f ty =
        let (b, o, n) = d50_parameter_type_extent ty
        in (ty, (b, b + o + n - 1))
  in map f d50_parameter_type_seq

{- | Split sequence into groups based on 'D50_Parameter_Type' sequence.
  Segments INCLUDE the NAME data segments.
-}
d50_parameter_segment :: [t] -> [(D50_Parameter_Type, [t])]
d50_parameter_segment p =
  let f (ty, (b, x)) = (ty, map (u24_at p) [b .. x])
  in map f d50_parameter_type_address_segments

-- | Determine 'D50_Parameter_Type' of value at 'D50_Address'.
d50_address_to_parameter_type :: D50_Address -> Maybe D50_Parameter_Type
d50_address_to_parameter_type a =
  let f ty = let (b, o, n) = d50_parameter_type_extent ty in a >= b && a < b + o + n
  in find f d50_parameter_type_seq

{- | Lookup parameter at address.

> let ix = [0 .. 420] in zip ix (map d50_address_to_parameter ix)
-}
d50_address_to_parameter :: D50_Address -> Maybe (D50_Parameter_Type, D50_Parameter)
d50_address_to_parameter a =
  case d50_address_to_parameter_type a of
    Just ty ->
      let (b, _, _) = d50_parameter_type_extent ty
          f (i, _, _, _, _) = i == a - b
      in fmap (\p -> (ty, p)) (find f (d50_parameters_by_type ty))
    Nothing -> Nothing

-- | Given 'D50_Parameter_Type' and name, lookup 'D50_Parameter'.
d50_parameter_lookup :: (D50_Parameter_Type, String) -> Maybe D50_Parameter
d50_parameter_lookup (ty, nm) =
  let f (_, nm', _, _, _) = nm == nm'
  in find f (d50_parameters_by_type ty)

{- | Get 'D50_Address' of named parameter.

> d50_named_parameter_to_address (Patch,"Lower Tone Fine Tune") == Just 409
> d50_named_parameter_to_address (Patch,"Key Mode") == Just 402
> d50_named_parameter_to_address (Common Lower,"Structure No.") == Just 330
> d50_named_parameter_to_address (Common Upper,"Partial Mute") == Just 174
-}
d50_named_parameter_to_address :: (D50_Parameter_Type, String) -> Maybe D50_Address
d50_named_parameter_to_address (ty, nm) =
  let f (n, _, _, _, _) = let (b, _, _) = d50_parameter_type_extent ty in b + n
  in fmap f (d50_parameter_lookup (ty, nm))

{- | The base address for the D50 work area memory.

4.2 Memory Area

[02-00-00] Patch Memory 1-1 = 0x08000
[02-03-40] Patch Memory 1-2 = 0x081C0
...
[03-5C-40] Patch Memory 8-8 = 0x0EE40
[03-60-00] Reverb Data 17   = 0x0F000
[03-62-78] Reverb Data 18   = 0x0F178
...
[04-0C-08] Reverb Data 32   = 0x10608
-}
d50_work_area_base_address :: D50_Address
d50_work_area_base_address = 0x8000

{- | Base address for patch memory /n/ (0,63).

> d50_addr_encode (0x02,0x00,0x00) == 0x8000 -- 32768
> d50_addr_encode (0x02,0x03,0x40) == 0x81C0 -- 33216
> d50_addr_encode (0x03,0x60,0x00) == 0xF000 -- 61440
> 33216 - 32768 == 0x01C0 -- 448
> 32768 + (448 * 64) == 0xF000 -- 61440
> 61440 - 448 == 0xEE40 -- 60992
> Midi.bits_21_sep_be 60992 == (0x03,0x5C,0x40)
-}
d50_patch_memory_base :: U8 -> D50_Address
d50_patch_memory_base n = d50_work_area_base_address + (d50_parameter_n * u8_to_u24 n)

-- * Reverb

-- | Size of stored (encoded) reverb data segment (376-bytes).
d50_reverb_data_segment_n :: Num n => n
d50_reverb_data_segment_n = 376

-- | Reverb data, 188-element byte-sequence (ie. decoded reverb data segment).
type D50_Reverb = [U8]

-- | Reverb data is 8-bit, but is stored in only the least significant 4-bits of each U8.
d50_reverb_join :: U8 -> U8 -> U8
d50_reverb_join d1 d2 =
  case (Midi.bits8_sep d1, Midi.bits8_sep d2) of
    ((0x0, d3), (0x0, d4)) -> Midi.bits4_join (d3, d4)
    _ -> error "d50_reverb_join?"

-- | Inverse of 'd50_reverb_join'.
d50_reverb_sep :: U8 -> (U8, U8)
d50_reverb_sep = Midi.bits8_sep

-- | Decode 376-element reverb block to 188-element reverb data.
d50_reverb_decode :: [U8] -> [U8]
d50_reverb_decode = map (uncurry d50_reverb_join) . List.adj2 2

-- | Inverse of 'd50_reverb_decode'
d50_reverb_encode :: [U8] -> [U8]
d50_reverb_encode =
  let recur x =
        case x of
          d : x' -> let (d1, d2) = d50_reverb_sep d in d1 : d2 : recur x'
          [] -> []
  in recur

{- | Base address for zero-indexed reverb memory /n/, ie. (0,15).
     The mutable reverbs are those numbered 17-32.
     Each reverb data segment is 376 bytes, the total reverb data segment is 6016 bytes.

> d50_addr_encode (0x03,0x60,0x00) == 61440
> d50_addr_encode (0x03,0x62,0x78) == 61816
> d50_addr_encode (0x04,0x0C,0x08) == 67080
> 61816 - 61440 == 376
> 61440 + (376 * (32 - 17)) == 67080
-}
d50_reverb_memory_base :: U8 -> D50_Address
d50_reverb_memory_base n = 61440 + (376 * u8_to_u24 n)

-- * Char

{- | Table mapping the (NON-ASCII) D-50 character encoding to it's ASCII character.

> length d50_char_table == 67
> map (\(x,c) -> (x,fromEnum c,c)) d50_char_table
-}
d50_char_table :: [(U8, Char)]
d50_char_table =
  let ch = [[' '], ['A' .. 'Z'], ['a' .. 'z'], ['1' .. '9'], ['0', '-', '?', '?', '-']]
  in zip [0 ..] (concat ch)

{- | Lookup in 'd50_char_table'.

> mapMaybe d50_byte_to_char [9,40,38,27,40,30] == "Inland"
-}
d50_byte_to_char :: U8 -> Maybe Char
d50_byte_to_char n = lookup n d50_char_table

-- | Erroring variant.
d50_byte_to_char_err :: U8 -> Char
d50_byte_to_char_err = fromMaybe (error "d50_byte_to_char") . d50_byte_to_char

{- | Reverse lookup in 'd50_char_table'.

> mapMaybe d50_char_to_byte "Inland" == [9,40,38,27,40,30]
-}
d50_char_to_byte :: Char -> Maybe U8
d50_char_to_byte c = List.reverse_lookup c d50_char_table

-- | Erroring variant.
d50_char_to_byte_err :: Char -> U8
d50_char_to_byte_err = fromMaybe (error "d50_char_to_byte?") . d50_char_to_byte

-- * Usr/Str

{- | Centering string spacer, bias SPACE to right if /rhs/ is True.

> let r = [[" 4  "," 34 ","234 ","1234"],["  4 "," 34 "," 234","1234"]]
> map (\rhs -> map (d50_str_pad_centre rhs 4) (words "4 34 234 1234")) [True,False] == r
-}
d50_str_pad_centre :: Bool -> Int -> String -> String
d50_str_pad_centre rhs k s =
  let n = length s
      (p1, p2) = (k - n) `divMod` 2
      p3 = p1 + p2
  in replicate (if rhs then p1 else p3) ' ' ++ s ++ replicate (if rhs then p3 else p1) ' '

{- | Pad first to left to n-spaces, and then to right to n+m spaces.

> map (d50_str_pad_lr 3 1) (words "0 5 10 20 100") == ["  0 ","  5 "," 10 "," 20 ","100 "]
-}
d50_str_pad_lr :: Int -> Int -> String -> String
d50_str_pad_lr n m s = List.pad_right ' ' (n + m) (List.pad_left ' ' n s)

-- | Strings naming the 12 pitch-classes. (1-2 CHAR)
d50_pitch_class_seq :: [String]
d50_pitch_class_seq = words "C C# D D# E F F# G G# A A# B"

{- | Strings naming the 84 pitches from C1 to B7. (2-3 CHAR)

> length d50_pitch_seq == 7 * 12
-}
d50_pitch_seq :: [String]
d50_pitch_seq = [p ++ show o | o <- [1 :: Int .. 7], p <- d50_pitch_class_seq]

-- | "C2 - C7" as USR string. (2-3 CHAR)
d50_split_point_usr :: D50_Usr_Str
d50_split_point_usr = intercalate ";" (take 61 (drop 12 d50_pitch_seq))

-- | "C1 - C7" as USR string. (2-3 CHAR)
d50_wg_pitch_coarse_usr :: D50_Usr_Str
d50_wg_pitch_coarse_usr = intercalate ";" (take 73 d50_pitch_seq)

-- | P-ENV KF USR string. (1-3 CHAR)
d50_wg_pitch_kf_usr :: D50_Usr_Str
d50_wg_pitch_kf_usr = "-1;-/2;-/4;0;1/8;1/4;3/8;1/2;5/6;3/4;7/8;1;5/4;3/2;2;s1;s2"

-- | Ratios and S form of WG KF.
d50_wg_pitch_kf_rat :: Fractional n => ([n], [String])
d50_wg_pitch_kf_rat = ([-1, 1 / 2, -1 / 4, 0, 1 / 8, 1 / 4, 3 / 8, 1 / 2, 5 / 6, 3 / 4, 7 / 8, 1, 5 / 4, 3 / 2, 2], ["S1", "S2"])

{- | Lookup ratio in 'wg_pitch_kf_rat'.

> mapMaybe d50_wg_pitch_kf_to_enum [1/8,1/4,1] == [4,5,11]
-}
d50_wg_pitch_kf_to_enum :: (Eq n, Fractional n) => n -> Maybe U8
d50_wg_pitch_kf_to_enum n = fmap int_to_u8_err (elemIndex n (fst d50_wg_pitch_kf_rat))

-- | Erroring variant.
d50_wg_pitch_kf_to_enum_err :: (Eq n, Fractional n) => n -> U8
d50_wg_pitch_kf_to_enum_err = fromMaybe (error "wg_pitch_kf_to_enum") . d50_wg_pitch_kf_to_enum

-- | TVF KF USR string.  (1-3 CHAR)
d50_tvf_kf_usr :: D50_Usr_Str
d50_tvf_kf_usr = "-1;-/2;-/4;0;1/8;1/4;3/8;1/2;5/8;3/4;7/8;1;5/4;3/2;2"

-- | "<A1 - <C7;>A1 - >C7". (3-4 CHAR)
d50_bias_point_direction_usr :: D50_Usr_Str
d50_bias_point_direction_usr =
  let p = take 64 (drop 9 d50_pitch_seq)
  in intercalate ";" (map ('<' :) p ++ map ('>' :) p)

-- | EQ LF values.
d50_eq_lf_seq :: Num n => [n]
d50_eq_lf_seq = [63, 75, 88, 105, 125, 150, 175, 210, 250, 300, 350, 420, 500, 600, 700, 840]

{- | EQ frequencies are written in hz for 0-999 and in khz from 1000-9999.

> map d50_eq_freq_show [1,10,100,150,1000,1500] == ["1","10","100","150","1.0","1.5"]
-}
d50_eq_freq_show :: Int -> String
d50_eq_freq_show n =
  let s = show n
  in if n >= 1000 then [s !! 0, '.', s !! 1] else s

-- | EQ LF USR string. (2-3 CHAR)
d50_eq_lf_usr :: D50_Usr_Str
d50_eq_lf_usr = intercalate ";" (map d50_eq_freq_show d50_eq_lf_seq)

-- | EQ HF values.
d50_eq_hf_seq :: Num n => [n]
d50_eq_hf_seq =
  [ 250
  , 300
  , 350
  , 420
  , 500
  , 600
  , 700
  , 840
  , 1000
  , 1200
  , 1400
  , 1700
  , 2000
  , 2400
  , 2800
  , 3400
  , 4000
  , 4800
  , 5700
  , 6700
  , 8000
  , 9500
  ]

-- | EQ HF USR string. (3 CHAR)
d50_eq_hf_usr :: D50_Usr_Str
d50_eq_hf_usr = intercalate ";" (map d50_eq_freq_show d50_eq_hf_seq)

d50_eq_q_seq :: Floating n => [n]
d50_eq_q_seq = [0.3, 0.5, 0.7, 1.0, 1.4, 2.0, 3.0, 4.2, 6.0]

-- | EQ Q USR string. (3 CHAR)
d50_eq_q_usr :: D50_Usr_Str
d50_eq_q_usr = intercalate ";" (map show (d50_eq_q_seq :: [Double]))

-- | /USR/ string indicating 'Char' enumeration sequence, ie. 'd50_char_table'.
d50_char_code_usr :: D50_Usr_Str
d50_char_code_usr = intersperse ';' (map snd d50_char_table)

-- | Key-mode ASCII string. (3-6 CHAR)
d50_key_mode_usr :: D50_Usr_Str
d50_key_mode_usr = "WHOLE;DUAL;SPLIT;SEP;WHOL-S;DUAL-S;SPL-US;SPL-LS;SEP-S"

{- * KEY MODE

WHOLE ;
 DUAL ;
SPLIT ;
 SEP  ; (Separate)
WHOL-S; (Whole Solo)
DUAL-S; (Dual Solo)
SPL-US; (Split Upper Solo)
SPL-LS; (Split Lower Solo)
SEP-S ; (Separate Solo)
-}
d50_key_mode_tbl :: [(U8, String)]
d50_key_mode_tbl = zip [0 ..] (Split.splitOn ";" d50_key_mode_usr)

-- | Is KEY-MODE 0=WHOLE or 4=WHOL-S?
d50_is_lower_tone_unused :: U8 -> Bool
d50_is_lower_tone_unused x = x == 0 || x == 4

-- * Structure

{- | Tone structure indicates synthesis type for each partial.
  Written as 3-character string (S=Synthesis, P=Pcm, R=Ring-Mod).
-}
d50_structure_usr :: D50_Usr_Str
d50_structure_usr = "SS-;SSR;PS-;PSR;SPR;PP-;PPR"

-- * Partial

-- | The number of PARTIAL parameters.
d50_partial_parameters_n :: Num n => n
d50_partial_parameters_n = 54

-- | Pcm Usr string. (3-6 CHAR)
d50_pcm_usr :: D50_Usr_Str
d50_pcm_usr = intercalate ";" (map (\(_, nm, _) -> dropWhileEnd (== ' ') nm) d50_pcm_wave_tbl_exp)

{- | Partial parameters (4.3).
  The indices in this table are zero based.
  The names are as written in the "D-50 MIDI Implementation" manual sections 4-4 and 4-5.

> length d50_partial_parameters == d50_partial_parameters_n
-}
d50_partial_parameters :: [D50_Parameter]
d50_partial_parameters =
  [ (0, "WG Pitch Coarse", 73, 0, d50_wg_pitch_coarse_usr)
  , (1, "WG Pitch Fine", 101, -50, "-50 - +50")
  , (2, "WG Pitch Keyfollow", 17, 0, d50_wg_pitch_kf_usr)
  , (3, "WG Mod LFO Mode", 4, 0, "OFF;(+);(~);A&L")
  , (4, "WG Mod P-ENV Mode", 3, 0, "OFF;(+);(-)")
  , (5, "WG Mod Bender Mode", 3, 0, "OFF;KF;NOM") -- NOM=NORMAL
  , (6, "WG Waveform", 2, 0, "SQU;SAW") -- S
  , (7, "WG PCM Wave No.", 100, 1, d50_pcm_usr) -- P (6-CHAR)
  , (8, "WG Pulse Width", 101, 0, "0 - 100") -- S
  , (9, "WG PW Velocity Range", 15, -7, "-7 - +7") -- S
  , (10, "WG PW LFO Select", 6, 0, "+1;-1;+2;-2;+3;-3") -- S
  , (11, "WG PW LFO Depth", 101, 0, "0 - 100") -- S
  , (12, "WG PW Aftertouch Range", 15, -7, "-7 - +7") -- S
  , (13, "TVF Cutoff Frequency", 101, 0, "0 - 100") -- S
  , (14, "TVF Resonance", 31, 0, "0 - 30") -- S
  , (15, "TVF Keyfollow", 15, 0, d50_tvf_kf_usr) -- S
  , (16, "TVF Bias Point/Direction", 128, 0, d50_bias_point_direction_usr) -- S (4-CHAR)
  , (17, "TVF Bias Level", 15, -7, "-7 - +7") -- S
  , (18, "TVF ENV Depth", 101, 0, "0 - 100") -- S
  , (19, "TVF ENV Velocity Range", 101, 0, "0 - 100") -- S
  , (20, "TVF ENV Depth Keyfollow", 5, 0, "0 - 4") -- S
  , (21, "TVF ENV Time Keyfollow", 11, 0, "0 - 10") -- S
  , (22, "TVF ENV Time 1", 101, 0, "0 - 100") -- S
  , (23, "TVF ENV Time 2", 101, 0, "0 - 100") -- S
  , (24, "TVF ENV Time 3", 101, 0, "0 - 100") -- S
  , (25, "TVF ENV Time 4", 101, 0, "0 - 100") -- S
  , (26, "TVF ENV Time 5", 101, 0, "0 - 100") -- S
  , (27, "TVF ENV Level 1", 101, 0, "0 - 100") -- S
  , (28, "TVF ENV Level 2", 101, 0, "0 - 100") -- S
  , (29, "TVF ENV Level 3", 101, 0, "0 - 100") -- S
  , (30, "TVF ENV Sustain Level", 101, 0, "0 - 100") -- S
  , (31, "TVF ENV End Level", 2, 0, "0;100") -- S
  , (32, "TVF Mod LFO Select", 6, 0, "+1;-1;+2;-2;+3;-3") -- S
  , (33, "TVF Mod LFO Depth", 101, 0, "0 - 100") -- S
  , (34, "TVF Mod Aftertouch Range", 15, -7, "-7 - +7") -- S
  , (35, "TVA Level", 101, 0, "0 - 100")
  , (36, "TVA Velocity Range", 101, -50, "-50 - +50")
  , (37, "TVA Bias Point/Direction", 128, 0, d50_bias_point_direction_usr) -- (4-CHAR)
  , (38, "TVA Bias Level", 13, -12, "-12 - 0")
  , (39, "TVA ENV Time 1", 101, 0, "0 - 100")
  , (40, "TVA ENV Time 2", 101, 0, "0 - 100")
  , (41, "TVA ENV Time 3", 101, 0, "0 - 100")
  , (42, "TVA ENV Time 4", 101, 0, "0 - 100")
  , (43, "TVA ENV Time 5", 101, 0, "0 - 100")
  , (44, "TVA ENV Level 1", 101, 0, "0 - 100")
  , (45, "TVA ENV Level 2", 101, 0, "0 - 100")
  , (46, "TVA ENV Level 3", 101, 0, "0 - 100")
  , (47, "TVA ENV Sustain Level", 101, 0, "0 - 100")
  , (48, "TVA ENV End Level", 2, 0, "0;100")
  , (49, "TVA ENV T1 Velo Follow", 5, 0, "0 - 4")
  , (50, "TVA ENV Time Keyfollow", 5, 0, "0 - 4")
  , (51, "TVA Mod LFO Select", 6, 0, "+1;-1;+2;-2;+3;-3") -- S
  , (52, "TVA Mod LFO Depth", 101, 0, "0 - 100") -- S
  , (53, "TVA Mod Aftertouch Range", 15, -7, "-7 - +7") -- S
  ]

{- | Parameter indices that apply only if Part is S/P.

> let (s,p) = d50_partial_SP_only in (length s,length p) == (31,1)
-}
d50_partial_SP_only :: ([U24], [U24])
d50_partial_SP_only = (concat [[6], [8 .. 34], [51 .. 53]], [7])

{- | D50 INIT-SQU partial data (PN-D50-01).

> length d50_partial_init_squ == length d50_partial_parameters
-}
d50_partial_init_squ :: [U8]
d50_partial_init_squ =
  [ 36
  , 50
  , 11
  , 1
  , 1
  , 1
  , 0
  , 0
  , 0
  , 7
  , 2
  , 0
  , 7 -- WG
  , 60
  , 0
  , 7
  , 27
  , 7
  , 0
  , 0
  , 0
  , 0
  , 0
  , 50
  , 50
  , 50
  , 50
  , 100
  , 100
  , 100
  , 100
  , 0
  , 2
  , 0
  , 7 -- TVF
  , 100
  , 60
  , 27
  , 12
  , 0
  , 50
  , 50
  , 50
  , 0
  , 100
  , 100
  , 100
  , 100
  , 0
  , 0
  , 0
  , 4
  , 0
  , 7 -- TVA
  ]

-- * Common (Tone)

-- | USR string for partial-mute value. (2 CHAR)
d50_partial_mute_usr :: D50_Usr_Str
d50_partial_mute_usr = "00;10;01;11" -- "MM;SM;MS;SS"

-- | The number of COMMON parameters, excluding the 10-character TONE NAME.
d50_common_factors_n :: Num n => n
d50_common_factors_n = 38

{- | Common parameters (4.4).
  This table is zero-indexed but does not include the 10-character tone name.

> length d50_common_factors == d50_common_factors_n
-}
d50_common_factors :: [D50_Parameter]
d50_common_factors =
  [ (10, "Structure No.", 7, 1, "1 - 7")
  , (11, "P-ENV Velocity Range", 3, 0, "0 - 2")
  , (12, "P-ENV Time Keyfollow", 5, 0, "0 - 4")
  , (13, "P-ENV Time 1", 51, 0, "0 - 50")
  , (14, "P-ENV Time 2", 51, 0, "0 - 50")
  , (15, "P-ENV Time 3", 51, 0, "0 - 50")
  , (16, "P-ENV Time 4", 51, 0, "0 - 50")
  , (17, "P-ENV Level 0", 101, -50, "-50 - +50")
  , (18, "P-ENV Level 1", 101, -50, "-50 - +50")
  , (19, "P-ENV Level 2", 101, -50, "-50 - +50")
  , (20, "P-ENV Sustain Level", 101, -50, "-50 - +50")
  , (21, "P-ENV End Level", 101, -50, "-50 - +50")
  , (22, "Pitch Mod LFO Depth", 101, 0, "0 - 100")
  , (23, "Pitch Mod Lever", 101, 0, "0 - 100")
  , (24, "Pitch Mod Aftertouch", 101, 0, "0 - 100")
  , (25, "LFO-1 Waveform", 4, 0, "TRI;SAW;SQU;RND")
  , (26, "LFO-1 Rate", 101, 0, "0 - 100")
  , (27, "LFO-1 Delay Time", 101, 0, "0 - 100")
  , (28, "LFO-1 Sync", 3, 0, "OFF;ON;KEY")
  , (29, "LFO-2 Waveform", 4, 0, "TRI;SAW;SQU;RND")
  , (30, "LFO-2 Rate", 101, 0, "0 - 100")
  , (31, "LFO-2 Delay Time", 101, 0, "0 - 100")
  , (32, "LFO-2 Sync", 2, 0, "OFF;ON")
  , (33, "LFO-3 Waveform", 4, 0, "TRI;SAW;SQU;RND")
  , (34, "LFO-3 Rate", 101, 0, "0 - 100")
  , (35, "LFO-3 Delay Time", 101, 0, "0 - 100")
  , (36, "LFO-S Sync", 2, 0, "OFF;ON")
  , (37, "Low EQ Frequency", 16, 0, d50_eq_lf_usr)
  , (38, "Low EQ Gain", 25, -12, "-12 - +12")
  , (39, "High EQ Frequency", 22, 0, d50_eq_hf_usr)
  , (40, "High EQ Q", 9, 0, d50_eq_q_usr)
  , (41, "High EQ Gain", 25, -12, "-12 - +12")
  , (42, "Chorus Type", 8, 1, "1 - 8")
  , (43, "Chorus Rate", 101, 0, "0 - 100")
  , (44, "Chorus Depth", 101, 0, "0 - 100")
  , (45, "Chorus Balance", 101, 0, "0 - 100")
  , (46, "Partial Mute", 4, 0, d50_partial_mute_usr)
  , (47, "Partial Balance", 101, 0, "0 - 100")
  ]

{- | D50 INIT-SQU common data (PN-D50-01).

> length (snd d50_common_init_squ) == length d50_common_factors
-}
d50_common_init_squ :: (String, [U8])
d50_common_init_squ =
  ( "-Init- SQU"
  ,
    [ 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 50
    , 50
    , 50
    , 50
    , 50
    , 0
    , 20
    , 0 -- STRUCT + P-ENV + P-MOD
    , 0
    , 75
    , 50
    , 0
    , 0
    , 50
    , 0
    , 0
    , 0
    , 30
    , 0
    , 0 -- LFO 1-3
    , 5
    , 12
    , 8
    , 0
    , 12
    , 0
    , 50
    , 50
    , 0 -- EQ + CHORUS
    , 1
    , 50 -- MUT + BAL
    ]
  )

-- * Patch

-- | The number of PATCH parameters, excluding 18-character PATCH NAME.
d50_patch_factors_n :: Num n => n
d50_patch_factors_n = 22

{- | 4.5 Patch Factors.
  This table is zero-indexed but does not include the 18-character patch name.

> length d50_patch_factors == d50_patch_factors_n
-}
d50_patch_factors :: [D50_Parameter]
d50_patch_factors =
  [ (18, "Key Mode", 9, 0, d50_key_mode_usr) -- 6-CHAR
  , (19, "Split Point", 61, 0, d50_split_point_usr)
  , (20, "Portamento Mode", 3, 0, "U;L;UL")
  , (21, "Hold Mode", 3, 0, "U;L;UL")
  , (22, "Upper Tone Key Shift", 49, -24, "-24 - +24")
  , (23, "Lower Tone Key Shift", 49, -24, "-24 - +24")
  , (24, "Upper Tone Fine Tune", 101, -50, "-50 - +50")
  , (25, "Lower Tone Fine Tune", 101, -50, "-50 - +50")
  , (26, "Bender Range", 13, 0, "0 - 12")
  , (27, "After Bend Range", 25, -12, "-12 - +12")
  , (28, "Portamento Time", 101, 0, "0 - 100")
  , (29, "Output Mode", 4, 1, "1 - 4")
  , (30, "Reverb Type", 32, 1, "1 - 32")
  , (31, "Reverb Balance", 101, 0, "0 - 100")
  , (32, "Total Volume", 101, 0, "0 - 100")
  , (33, "Tone Balance", 101, 0, "0 - 100")
  , (34, "Chase Mode", 3, 0, "UL;ULL;ULU")
  , (35, "Chase Level", 101, 0, "0 - 100")
  , (36, "Chase Time", 101, 0, "0 - 100")
  , (37, "Midi Transmit Channel", 17, 0, "BASIC-CH | 1 - 16")
  , (38, "Midi Separate Rcv Channel", 17, 0, "OFF | 1 - 16")
  , (39, "Midi Transmit Prog. Change", 101, 0, "OFF | 1 - 100")
  ]

-- * Parameter

{- | Complete set of /used/ 'D50_Parameter's.
  Excludes NAME data.
  The table is grouped U1,U2,U,L1,L2,L,P and is zero-indexed and is sparse.

> map length d50_parameters == [54,54,38,54,54,38,22]
> length (concat d50_parameters) == 314
> nub $ sort $ map d50_parameter_user_offset $ concat d50_parameters
> putStrLn $ unlines $ map show $ concat d50_parameters
-}
d50_parameters :: [[D50_Parameter]]
d50_parameters =
  let f i = map (\(ix, nm, stp, usr_diff, usr_str) -> (ix + i, nm, stp, usr_diff, usr_str))
      a = map d50_parameter_type_base_address d50_parameter_type_seq
      p =
        [ d50_partial_parameters
        , d50_partial_parameters
        , d50_common_factors
        , d50_partial_parameters
        , d50_partial_parameters
        , d50_common_factors
        , d50_patch_factors
        ]
  in zipWith f a p

-- | The number of D50 patch parameters, including NAMES and unused.
d50_parameter_n :: Num n => n
d50_parameter_n = 448

-- | A D50 patch is a 448-byte sequence.
type D50_Patch = [U8]

-- | D50 param in sequence: U1 U2 U L1 L2 L P.  Excludes NAME data.
type D50_Param = [[U8]]

-- | A list of differences, ie. (ADDRESS,VALUE) pairs.
type D50_Diff = [(D50_Address, U8)]

-- | Apply 'D50_Diff' to 'D50_Patch'.
d50_diff_apply :: D50_Patch -> D50_Diff -> D50_Patch
d50_diff_apply p d = map snd (List.merge_by_resolve const (compare `on` fst) (sort d) (zip [0 ..] p))

{- | Number of D50 non-name parameters.

> d50_param_n == 314
-}
d50_param_n :: Num n => n
d50_param_n = d50_partial_parameters_n * 4 + d50_common_factors_n * 2 + d50_patch_factors_n

{- | Degree of each element of 'D50_Param'.

> sum d50_param_places == d50_param_n
-}
d50_param_places :: [Int]
d50_param_places = [54, 54, 38, 54, 54, 38, 22]

-- | Split sequence of 'd50_param_n' U8 into 'D50_Param'
d50_param_segment :: [U8] -> D50_Param
d50_param_segment = Split.splitPlaces d50_param_places

-- | Check D50_Param has correct number of elements (314).
d50_param_verify :: D50_Param -> Bool
d50_param_verify = (==) d50_param_places . map length

{- | Addresses for 'D50_Param'

> map length d50_patch_param_addr == d50_param_places
-}
d50_patch_param_addr :: [[D50_Address]]
d50_patch_param_addr =
  let f x (_, (p, q)) = drop x [p .. q]
  in zipWith f [0, 0, 10, 0, 0, 10, 18] d50_parameter_type_address_segments

-- | 'D50_Param' of 'D50_Patch'.
d50_patch_param :: D50_Patch -> D50_Param
d50_patch_param p =
  let f = map (u24_at p)
  in map f d50_patch_param_addr

-- | Make (ADDRESS,VALUE) sequence for 'D50_Param'
d50_patch_param_cons :: D50_Param -> [(D50_Address, U8)]
d50_patch_param_cons p = zip (concat d50_patch_param_addr) (concat p)

-- | Make (ADDRESS,VALUE) sequence for 'D50_Patch_Name_Set' and 'D50_Param'
d50_patch_cons :: D50_Patch_Name_Set -> D50_Param -> [(D50_Address, U8)]
d50_patch_cons nm pr = sort (d50_patch_name_set_cons nm ++ d50_patch_param_cons pr)

-- | Make 'D50_Patch' given 'D50_Patch_Name_Set' and 'D50_Param'
d50_patch_gen :: D50_Patch_Name_Set -> D50_Param -> D50_Patch
d50_patch_gen nm pr = d50_diff_apply (replicate d50_parameter_n 0) (d50_patch_cons nm pr)

-- | Construct parameter for character in a name.
d50_name_parameter :: U24 -> D50_Parameter
d50_name_parameter ix = (ix, "NAME CHAR " ++ show ix, 64, 0, d50_char_code_usr)

{- | The D50 parameter address space is sparse, ie. has unused addresses.
  Construct an UNUSED parameter.
-}
d50_unused_parameter :: U24 -> D50_Parameter
d50_unused_parameter ix = (ix, "UNUSED", 1, 0, "")

{- | Complete 'D50_Parameter' sequence, including all /unused/ parameters.

> length d50_parameters_seq == d50_parameter_n
> map d50_parameter_ix d50_parameters_seq == [0 .. d50_parameter_n - 1]
-}
d50_parameters_seq :: [D50_Parameter]
d50_parameters_seq =
  let recur n sq =
        case sq of
          [] -> []
          p : sq' ->
            if n == d50_parameter_ix p
              then p : recur (n + 1) sq'
              else
                let x =
                      if d50_is_name_address n
                        then d50_name_parameter n
                        else d50_unused_parameter n
                in x : recur (n + 1) sq
  in recur 0 (concat d50_parameters) ++ map d50_unused_parameter [424 .. 447]

-- | Parameters of indicated type.
d50_parameters_by_type :: D50_Parameter_Type -> [D50_Parameter]
d50_parameters_by_type ty =
  case ty of
    Partial _ _ -> d50_partial_parameters
    Common _ -> d50_common_factors
    Patch -> d50_patch_factors

-- * Parameter

-- | Parameter range as stored.
d50_parameter_range :: D50_Parameter -> (U8, U8)
d50_parameter_range (_, _, stp, _, _) = (0, stp - 1)

-- | Parameter range as displayed (USR).
d50_parameter_range_usr :: D50_Parameter -> (I8, I8)
d50_parameter_range_usr (_, _, stp, diff, _) = (diff, diff + u8_to_i8_err stp - 1)

-- | Verify that /x/ is a valid value for parameter, ie. identity or error.
d50_parameter_value_verify :: D50_Parameter -> U8 -> U8
d50_parameter_value_verify (_, _, stp, _, _) x =
  if x < 0 || x > stp
    then error (show ("parameter_value_verify", x, stp))
    else x

-- | Make USR display value for value /x/ at parameter /p/.
d50_parameter_value_usr :: D50_Parameter -> U8 -> Maybe String
d50_parameter_value_usr p x =
  let (_, _, stp, usr_diff, usr_str) = p
  in if x < 0 || x > stp
      then Nothing
      else Just (d50_usr_ix (show (u8_to_i8_err x + usr_diff)) usr_str x)

-- | Variant that allows a default string for cases where the value is out of range.
d50_parameter_value_usr_def :: String -> D50_Parameter -> U8 -> String
d50_parameter_value_usr_def def p = fromMaybe def . d50_parameter_value_usr p

-- | Variant that errors if the value is out of range.
d50_parameter_value_usr_err :: D50_Parameter -> U8 -> String
d50_parameter_value_usr_err p x =
  fromMaybe
    (error (show ("parameter_value_usr", p, x)))
    (d50_parameter_value_usr p x)

-- * Patch

-- | Patch name, 18-byte ASCII string.
d50_patch_name :: D50_Patch -> String
d50_patch_name = map d50_byte_to_char_err . take 18 . u24_drop (d50_parameter_type_base_address Patch)

{- | Tone name, 10-byte ASCII string.

> tone_name Upper p
-}
d50_tone_name :: Tone -> D50_Patch -> String
d50_tone_name t =
  map d50_byte_to_char_err
    . take 10
    . u24_drop (d50_parameter_type_base_address (Common t))

-- | (UPPER-TONE-NAME,LOWER-TONE-NAME,PATCH-NAME), ie. in address sequence.
type D50_Patch_Name_Set = (String, String, String)

{- | Empty names, all characters are /c/.

> d50_patch_name_set_nil '-' == ("----------","----------","------------------")
-}
d50_patch_name_set_nil :: Char -> D50_Patch_Name_Set
d50_patch_name_set_nil c = (replicate 10 c, replicate 10 c, replicate 18 c)

{- | Addresses for 'D50_Patch_Name_Set'

> sum (map length d50_patch_name_set_addr) == 38
-}
d50_patch_name_set_addr :: [[D50_Address]]
d50_patch_name_set_addr =
  let i = map d50_parameter_type_base_address [Common Upper, Common Lower, Patch]
      j = [10, 10, 18]
  in zipWith (\x y -> [x .. x + y - 1]) i j

{- | Make (ADDRESS,VALUE) set for 'D50_Patch_Name_Set'

> d50_diff_csv_e (d50_patch_name_set_cons (d50_patch_name_set_nil '-'))
-}
d50_patch_name_set_cons :: D50_Patch_Name_Set -> [(D50_Address, U8)]
d50_patch_name_set_cons (u, l, p) =
  zip
    (concat d50_patch_name_set_addr)
    (map d50_char_to_byte_err (concat [u, l, p]))

-- | 'D50_Patch_Name_Set' of 'D50_Patch'.
d50_patch_name_set :: D50_Patch -> D50_Patch_Name_Set
d50_patch_name_set p = (d50_tone_name Upper p, d50_tone_name Lower p, d50_patch_name p)

{- | Match names using 'isInfixOf', case-insenstive if /cs/ is False.
  Empty names match everything, so ("","",p) matches only on /p/.
-}
d50_patch_name_match :: Bool -> D50_Patch_Name_Set -> D50_Patch_Name_Set -> Bool
d50_patch_name_match cs (u1, l1, p1) (u2, l2, p2) =
  let c x = if cs then x else map toLower x
      m x y = c x `isInfixOf` c y
  in m u1 u2 && m l1 l2 && m p1 p2

-- * SysEx

-- | SYSEX as sequence of U8.
type D50_SysEx = [U8]

{- | Segment byte-sequence into SYSEX messages, no verification,
ie. separate before each @0xF0@.
-}
d50_sysex_segment :: [U8] -> [D50_SysEx]
d50_sysex_segment = List.tail_err . List.split_before 0xF0

-- * D-50 SysEx Cmd

-- | D50 SysEx command id.
data D50_SysEx_Cmd
  = -- | REQUEST DATA - ONE WAY (0x11)
    Rq1_Cmd
  | -- | DATA SET - ONE WAY (0x12)
    Dt1_Cmd
  | -- | WANT TO SEND DATA (0x40)
    Wsd_Cmd
  | -- | REQUEST DATA (0x41)
    Rqd_Cmd
  | -- | DATA SET (0x42)
    Dat_Cmd
  | -- | ACKNOWLEDGE (0x43)
    Ack_Cmd
  | -- | END OF DATA (0x45)
    Eod_Cmd
  | -- | COMMUNICATION ERROR (0x4E)
    Err_Cmd
  | -- | REJECTION (0x4F)
    Rjc_Cmd
  deriving (Eq, Show)

-- | Table mapping 'D50_SysEx_Cmd' to it's 'U8' code.
d50_sysex_cmd_tbl :: [(D50_SysEx_Cmd, U8)]
d50_sysex_cmd_tbl =
  [ (Rq1_Cmd, 0x11)
  , (Dt1_Cmd, 0x12)
  , (Wsd_Cmd, 0x40)
  , (Rqd_Cmd, 0x41)
  , (Dat_Cmd, 0x42)
  , (Ack_Cmd, 0x43)
  , (Eod_Cmd, 0x45)
  , (Err_Cmd, 0x4E)
  , (Rjc_Cmd, 0x4F)
  ]

{- | SysEx_Cmd to 8-bit identifier.

> let k = d50_sysex_cmd_encode Dt1_Cmd in (k == 0x12,k == 0b00010010)
> let k = d50_sysex_cmd_encode Rq1_Cmd in (k == 0x11,k == 0b00010001)
-}
d50_sysex_cmd_encode :: D50_SysEx_Cmd -> U8
d50_sysex_cmd_encode cmd = Maybe.from_just "d50_sysex_cmd_encode" (lookup cmd d50_sysex_cmd_tbl)

-- | Inverse of 'd50_sysex_cmd_encode'.
d50_sysex_cmd_decode :: U8 -> D50_SysEx_Cmd
d50_sysex_cmd_decode i = Maybe.from_just "d50_sysex_cmd_decode" (List.reverse_lookup i d50_sysex_cmd_tbl)

{- | D50 device ID.

> :set -XBinaryLiterals
> (d50_id == 0x14,d50_id == 0b00010100)
-}
d50_id :: U8
d50_id = 0x14

{- | D50 SYSEX header, 5-byte sequence of:

F0 - Exclusive status
41 - Roland-ID
CH - Channel (Device-ID)
14 - Model-ID
Cmd - Command-ID
-}
d50_cmd_hdr :: U8 -> D50_SysEx_Cmd -> [U8]
d50_cmd_hdr ch cmd = [Constant.k_SysEx_Status, Constant.k_Roland_Id, ch, d50_id, d50_sysex_cmd_encode cmd]

-- | Data with checksum and F7 appended.
d50_cmd_data_chk :: [U8] -> [U8]
d50_cmd_data_chk dat = dat ++ [roland_checksum dat, Constant.k_SysEx_End]

-- | Generate Addr/Size category of D-50 SysEx messages.
d50_addr_sz_cmd_gen :: D50_SysEx_Cmd -> U8 -> D50_Address -> U24 -> D50_SysEx
d50_addr_sz_cmd_gen cmd ch addr sz =
  let ((d1, d2, d3), (d4, d5, d6)) = (d50_addr_decode addr, d50_addr_decode sz)
  in concat [d50_cmd_hdr ch cmd, d50_cmd_data_chk [d1, d2, d3, d4, d5, d6]]

{- | Generate Wsd command SysEx.

> let pp = T.byte_seq_hex_pp True
> let syx = T.read_hex_byte_seq_ws "F0 41 00 14 40 02 00 00 02 0F 00 6D F7"
> d50_wsd_gen 0 0x8000 0x8780 == syx
> d50_cmd_parse syx == Just (0,Wsd_Cmd,[2,0,0,2,15,0],6)
-}
d50_wsd_gen :: U8 -> D50_Address -> U24 -> D50_SysEx
d50_wsd_gen = d50_addr_sz_cmd_gen Wsd_Cmd

-- | Generate Rqd command sysex.
d50_rqd_gen :: U8 -> D50_Address -> U24 -> D50_SysEx
d50_rqd_gen = d50_addr_sz_cmd_gen Rqd_Cmd

{- | Generate RQI command SYSEX.

3.1 Request (One way) RQ1 11H

a 1111 0000 F0 Exclusive status
b 0100 0001 41 Roland-ID
c 0000 nnnn    Device-ID = MIDI CHANNEL
d 0001 0100 14 Model-ID
e 0001 0001 11 Command-ID
f 0aaa aaaa    Address MSB
g 0bbb bbbb    Address
h 0ccc cccc    Address LSB
i 0ddd dddd    Size MSB
j 0eee eeee    Size
k 0fff ffff    Size LSB
l 0ggg gggg    Checksum
m 1111 0111 F7 End of System Exclusive

Address: [00-00-00]
Size: [00-03-25] (421-bytes)

> let r = map T.read_hex_byte_err (words "F0 41 00 14 11 00 00 00 00 03 40 3D F7")
> d50_rq1_gen 0 0 0x1C0 == r
-}
d50_rq1_gen :: U8 -> D50_Address -> U24 -> D50_SysEx
d50_rq1_gen = d50_addr_sz_cmd_gen Rq1_Cmd

{- | Make Ack sysex.

> pp (d50_ack_gen 0) == "F0 41 00 14 43 F7"
> d50_cmd_parse (d50_ack_gen 0) == Just (0,Ack_Cmd,[],0)
-}
d50_ack_gen :: U8 -> D50_SysEx
d50_ack_gen ch = d50_cmd_hdr ch Ack_Cmd ++ [Constant.k_SysEx_End]

{- | Make EOD sysex.

> pp (d50_eod_gen 0) == "F0 41 00 14 45 F7"
> d50_cmd_parse (d50_eod_gen 0) == Just (0,Eod_Cmd,[],0)
-}
d50_eod_gen :: U8 -> D50_SysEx
d50_eod_gen ch = d50_cmd_hdr ch Eod_Cmd ++ [Constant.k_SysEx_End]

{- | Make RJC sysex.

> pp (d50_rjc_gen 0) == "F0 41 00 14 4F F7"
> d50_cmd_parse (d50_rjc_gen 0) == Just (0,0x4F,[],0)
-}
d50_rjc_gen :: U8 -> D50_SysEx
d50_rjc_gen ch = d50_cmd_hdr ch Rjc_Cmd ++ [Constant.k_SysEx_End]

{- | Parse SysEx to (Ch,Cmd,Dat,#Dat).

> d50_cmd_parse (d50_dsc_gen (Dt1_Cmd,0,1,[50])) == Just (0,Dt1_Cmd,[0,0,1,50],4)
-}
d50_cmd_parse :: D50_SysEx -> Maybe (U8, D50_SysEx_Cmd, [U8], U24)
d50_cmd_parse b =
  let b0 : b1 : b2 : b3 : b4 : b' = b
      dat_chk_n = u24_length b' - 1
      (dat_chk, [eox]) = u24_split_at dat_chk_n b'
      cmd = d50_sysex_cmd_decode b4
  in if any not [b0 == Constant.k_SysEx_Status, b1 == Constant.k_Roland_Id, b3 == d50_id, eox == Constant.k_SysEx_End]
      then Nothing
      else
        if dat_chk_n == 0
          then Just (b2, cmd, [], 0)
          else
            let Just (dat, dat_n) = d50_data_chk (dat_chk, dat_chk_n)
            in Just (b2, cmd, dat, dat_n)

{- | Check and remove CHECKSUM from DATA.

> d50_data_chk ([0,0,1,50,77],5) == Just ([0,0,1,50],4)
-}
d50_data_chk :: ([U8], U24) -> Maybe ([U8], U24)
d50_data_chk (dat_chk, dat_chk_n) =
  let (dat, [chk]) = u24_split_at (dat_chk_n - 1) dat_chk
  in if roland_checksum dat == chk
      then Just (dat, dat_chk_n - 1)
      else Nothing

{- | Remove and pack ADDRESS from start of DAT.

> d50_data_addr ([0,0,1,50],4) == Just (1,[50],1)
-}
d50_data_addr :: ([U8], U24) -> Maybe (U24, [U8], U24)
d50_data_addr (dat, dat_n) =
  case dat of
    a0 : a1 : a2 : dat' -> Just (d50_addr_encode (a0, a1, a2), dat', dat_n - 3)
    _ -> Nothing

-- | Parse addr/size command from SYSEX.
d50_addr_sz_cmd_parse :: D50_SysEx -> Maybe (U8, D50_SysEx_Cmd, D50_Address, U24)
d50_addr_sz_cmd_parse syx =
  case d50_cmd_parse syx of
    Just (ch, cmd, [a1, a2, a3, s1, s2, s3], 6) -> Just (ch, cmd, d50_addr_encode (a1, a2, a3), d50_addr_encode (s1, s2, s3))
    _ -> Nothing

-- * D50 Dsc SysEx (Dt1|Dat)

-- | Dsc = Data-set Command (Dt1|Dat), (Cmd,Device-Id,Address,Data)
type D50_Dsc = (D50_SysEx_Cmd, U8, D50_Address, [U8])

-- | ADDR field of DSC.
d50_dsc_address :: D50_Dsc -> D50_Address
d50_dsc_address (_, _, a, _) = a

-- | DATA field of DSC.
d50_dsc_data :: D50_Dsc -> [U8]
d50_dsc_data (_, _, _, d) = d

-- | Set the CMD element of DSC to /cmd/.
d50_dsc_set_cmd :: D50_SysEx_Cmd -> D50_Dsc -> D50_Dsc
d50_dsc_set_cmd cmd (_, ch, a, d) = (cmd, ch, a, d)

{- | Parse DSC (DT1|DAT) SYSEX message.

> let b = d50_dsc_gen (Dt1_Cmd,0,1,[50])
> d50_dsc_parse b == Just (Dt1_Cmd,0,1,[50])
-}
d50_dsc_parse :: D50_SysEx -> Maybe D50_Dsc
d50_dsc_parse syx =
  let Just (ch, cmd, addr_dat, addr_dat_n) = d50_cmd_parse syx
      Just (addr, dat, _dat_n) = d50_data_addr (addr_dat, addr_dat_n)
  in if cmd == Dt1_Cmd || cmd == Dat_Cmd
      then Just (cmd, ch, addr, dat)
      else Nothing

-- | Erroring variant.
d50_dsc_parse_err :: D50_SysEx -> D50_Dsc
d50_dsc_parse_err = fromMaybe (error "d50_dsc_parse") . d50_dsc_parse

{- | Generate Dsc (Dt1|Dat) SysEx Message.

> d50_dsc_gen (Dt1_Cmd,0,1,[50]) == T.read_hex_byte_seq "F041001412000001324DF7"
> d50_dsc_gen (Dt1_Cmd,0,1,[50]) == T.read_hex_byte_seq "F041001412000001324DF7"
-}
d50_dsc_gen :: D50_Dsc -> D50_SysEx
d50_dsc_gen (cmd, ch, a, d) =
  let (d1, d2, d3) = d50_addr_decode a
  in if length d > 256
      then error "d50_dsc_gen: #DATA > 256"
      else concat [d50_cmd_hdr ch cmd, d50_cmd_data_chk (d1 : d2 : d3 : d)]

-- | Generate a sequence of DSC messages segmenting data sets longer than 256 elements.
d50_dsc_gen_seq :: D50_Dsc -> [D50_SysEx]
d50_dsc_gen_seq (cmd, ch, a, d) =
  if null d
    then []
    else
      if length d <= 256
        then [d50_dsc_gen (cmd, ch, a, d)]
        else d50_dsc_gen (cmd, ch, a, take 256 d) : d50_dsc_gen_seq (cmd, ch, a + 256, drop 256 d)

{- | 'd50_dsc_gen' of 'd50_named_parameter_to_address'.

3.2 Data set (One way) DT1 12H

a 1111 0000 F0 Exclusive status
b 0100 0001 41 Roland-ID
c 0000 nnnn    Device-ID = MIDI CHANNEL
d 0001 0100 14 Model-ID
e 0001 0010 12 Command-ID
f 0aaa aaaa    Address MSB
g 0bbb bbbb    Address
h 0ccc cccc    Address LSB
i 0ddd dddd    Data
...
j 0eee eeee    Checksum
k 1111 0111 F7 End of System Exclusive

> let nm = (Patch,"Lower Tone Fine Tune")
> d50_gen_dt1_nm 0 nm [0x10] == Just (T.read_hex_byte_seq "F0410014120003191054F7")
-}
d50_gen_dt1_nm :: U8 -> (D50_Parameter_Type, String) -> [U8] -> Maybe D50_SysEx
d50_gen_dt1_nm ch nm d =
  let f a = d50_dsc_gen (Dt1_Cmd, ch, a, d)
  in fmap f (d50_named_parameter_to_address nm)

-- * D50 Dsc Seq

-- | Join a contiguous sequence of 'D50_Dsc' into one data segment, ie. (address,size,data).
d50_dsc_seq_join :: [D50_Dsc] -> (D50_Address, U24, [U8])
d50_dsc_seq_join sq =
  let addr_seq = map d50_dsc_address sq
      dat_seq = map d50_dsc_data sq
      size_seq = map u24_length dat_seq
      addr : _ = addr_seq
  in if List.d_dx addr_seq `isPrefixOf` size_seq
      then (addr, sum size_seq, concat dat_seq)
      else error "d50_dsc_seq_join?"

-- | If data segment is a D50 bulk-data transfer partition into 64 patches and 16 reverbs.
d50_bulk_data_transfer_parse :: (D50_Address, U24, [U8]) -> Maybe ([D50_Patch], [D50_Reverb])
d50_bulk_data_transfer_parse (addr, sz, dat) =
  let patch_n = d50_parameter_n * 64
      rvb_n = d50_reverb_data_segment_n * 16
  in if addr /= 0x8000 || sz /= (patch_n + rvb_n)
      then Nothing
      else
        let (p_dat, r_dat) = u24_split_at patch_n dat
        in Just
            ( Split.chunksOf d50_parameter_n p_dat
            , map d50_reverb_decode (Split.chunksOf d50_reverb_data_segment_n r_dat)
            )

-- * Tuning

{- | Generate Dsc/Dt1 sequence to set the @Wg Pitch Kf@ parameters of all
four partials to indicated ratio.

> map d50_dsc_gen (d50_wg_pitch_kf_dt1 (1/4))
-}
d50_wg_pitch_kf_dt1 :: (Eq n, Fractional n) => n -> [D50_Dsc]
d50_wg_pitch_kf_dt1 r =
  let e = d50_wg_pitch_kf_to_enum_err r
      a_seq = map (+ 2) d50_partial_base_address_seq
      f a = (Dt1_Cmd, 0, a, [e])
  in map f a_seq

-- * Hex I/O

{- | Load text file where each line is a sequence of 448 (0x1C0)
   two-character hexadecimal byte values, ie. a D50 patch.
-}
d50_load_hex :: FilePath -> IO [D50_Patch]
d50_load_hex fn = do
  b <- Byte.load_hex_byte_seq fn
  case b of
    [] -> error "d50_load_hex?"
    x -> if any ((/=) 448 . length) x then error "d50_load_hex: 448?" else return x

-- | Type specialised 'T.store_hex_byte_seq'
d50_store_hex :: FilePath -> [D50_Patch] -> IO ()
d50_store_hex = Byte.store_hex_byte_seq

-- * Binary I/O

-- | Load binary 'U8' sequence from file.
d50_load_binary_u8 :: FilePath -> IO [U8]
d50_load_binary_u8 = Midi.bytes_load

-- | Write binary 'U8' sequence to file.
d50_store_binary_u8 :: FilePath -> [U8] -> IO ()
d50_store_binary_u8 = Midi.bytes_store

{- | Load Dt1|Dat sequence from 36048-byte D-50 SysEx file.

> let sysex_fn = "/home/rohan/sw/hsc3-data/data/roland/d50/PN-D50-00.syx"
> b <- d50_load_binary_u8 sysex_fn
> let s = d50_sysex_segment b
> d <- d50_load_sysex_dsc sysex_fn
> let s' = d50_dsc_gen_seq (Dt1_Cmd,0,d50_patch_memory_base 0,concatMap d50_dsc_data d)
> s == s'
-}
d50_load_sysex_dsc :: FilePath -> IO [D50_Dsc]
d50_load_sysex_dsc fn = do
  b <- d50_load_binary_u8 fn
  let b_n = length b
  when (b_n /= 0x8CD0) (putStrLn "d50_load_sysex: sysex != 0x8CD0 (36048)")
  when (b_n < 0x8CD0) (error "d50_load_sysex: sysex < 0x8CD0 (36048)")
  return (map d50_dsc_parse_err (d50_sysex_segment b))

{- | Load patch data (64 448-byte patches) and reverb data (16 decoded
  188-byte reverbs) from D-50 SYSEX file.

> let sysex_fn = "/home/rohan/sw/hsc3-data/data/roland/d50/PN-D50-00.syx"
> (p,r) <- d50_load_sysex sysex_fn
> putStrLn $ unlines $ map d50_patch_name p
-}
d50_load_sysex :: FilePath -> IO ([D50_Patch], [D50_Reverb])
d50_load_sysex fn = do
  dsc <- d50_load_sysex_dsc fn
  case d50_bulk_data_transfer_parse (d50_dsc_seq_join dsc) of
    Just r -> return r
    Nothing -> error "d50_load_sysex?"

{-

let sysex_fn = "/home/rohan/sw/hsc3-data/data/roland/d50/PN-D50-00.syx"
(p,_) <- d50_load_sysex sysex_fn
p0 = p !! 0
n = d50_patch_name_set p0
r = d50_patch_param p0
import Sound.Sc3.Data.Roland.D50.PP
d50_diff_csv_e (d50_patch_name_set_cons n)
map (d50_parameter_name . u24_at d50_parameters_seq) (map fst (d50_patch_param_cons r))
d50_diff_csv_e (d50_patch_param_cons r)
d50_patch_group_pp (d50_patch_gen n r)

p29 = p !! 29
d50_patch_name_set_pp p29
d50_parameter_segment p29

-}
