-- | <http://www.csounds.com/manual/html/MiscModalFreq.html>
module Sound.Sc3.Data.Modal where

{- | Table of modal frequency ratios for specified sound sources.

> import Sound.Sc3
> let f = 221
> let Just r = lookup "Tibetan bowl (180mm)" modal_frequency_ratios
> let u n = replicate (length r) n
> let k = klankSpec (map (* f) r) (u 1) (u 16)
> audition (out 0 (klank (impulse ar 0.125 0 * 0.1) 1 0 1 k))
-}
modal_frequency_ratios :: Fractional n => [(String, [n])]
modal_frequency_ratios =
  [ ("Dahina tabla", [1, 2.89, 4.95, 6.99, 8.01, 9.02])
  , ("Bayan tabla", [1, 2.0, 3.01, 4.01, 4.69, 5.63])
  , ("Red Cedar wood plate", [1, 1.47, 2.09, 2.56])
  , ("Redwood wood plate", [1, 1.47, 2.11, 2.57])
  , ("Douglas Fir wood plate", [1, 1.42, 2.11, 2.47])
  , ("Uniform wooden bar", [1, 2.572, 4.644, 6.984, 9.723, 12])
  , ("Uniform aluminum bar", [1, 2.756, 5.423, 8.988, 13.448, 18.680])
  , ("Xylophone", [1, 3.932, 9.538, 16.688, 24.566, 31.147])
  , ("Vibraphone 1", [1, 3.984, 10.668, 17.979, 23.679, 33.642])
  , ("Vibraphone 2", [1, 3.997, 9.469, 15.566, 20.863, 29.440])
  , ("Chalandi plates", [1, 1.72581, 5.80645, 7.41935, 13.91935])
  , ("Tibetan bowl (180mm)", [1, 2.77828, 5.18099, 8.16289, 11.66063, 15.63801, 19.99])
  , ("Tibetan bowl (152mm)", [1, 2.66242, 4.83757, 7.51592, 10.64012, 14.21019, 18.14027])
  , ("Tibetan bowl (140mm)", [1, 2.76515, 5.12121, 7.80681, 10.78409])
  , ("Wine Glass", [1, 2.32, 4.25, 6.63, 9.38])
  , ("Small handbell", [1, 1.0019054878049, 1.7936737804878, 1.8009908536585, 2.5201981707317, 2.5224085365854, 2.9907012195122, 2.9940548780488, 3.7855182926829, 3.8061737804878, 4.5689024390244, 4.5754573170732, 5.0296493902439, 5.0455030487805, 6.0759908536585, 5.9094512195122, 6.4124237804878, 6.4430640243902, 7.0826219512195, 7.0923780487805, 7.3188262195122, 7.5551829268293])
  , ("Spinel sphere (diameter=3.6675mm)", [1, 1.026513174725, 1.4224916858532, 1.4478690202098, 1.4661959580455, 1.499452545408, 1.7891839345101, 1.8768994627782, 1.9645945254541, 1.9786543873113, 2.0334612432847, 2.1452852391916, 2.1561524686621, 2.2533435661294, 2.2905090816065, 2.3331798413917, 0, 2.4567715528268, 2.4925556408289, 2.5661806088514, 2.6055768738808, 2.6692760296751, 2.7140956766436, 2.7543617293425, 2.7710411870043])
  , ("Pot lid", [1, 3.2, 6.23, 6.27, 9.92, 14.15])
  ]

{- | Table of modal frequencies for subset of 'modal_frequency_ratios'.

> import Sound.Sc3
> let Just f = lookup "Spinel sphere (diameter=3.6675mm)" modal_frequencies
> let u n = replicate (length f) n
> let k = klankSpec f (u 1) (u 16)
> audition (out 0 (klank (impulse ar 0.125 0 * 0.1) 1 0 1 k))
-}
modal_frequencies :: Fractional n => [(String, [n])]
modal_frequencies =
  [ ("Chalandi plates", [62, 107, 360, 460, 863])
  , ("Tibetan bowl (180mm)", [221, 614, 1145, 1804, 2577, 3456, 4419])
  , ("Tibetan bowl (152mm)", [314, 836, 1519, 2360, 3341, 4462, 5696])
  , ("Tibetan bowl (140mm)", [528, 1460, 2704, 4122, 5694])
  , ("Small handbell", [1312.0, 1314.5, 2353.3, 2362.9, 3306.5, 3309.4, 3923.8, 3928.2, 4966.6, 4993.7, 5994.4, 6003.0, 6598.9, 6619.7, 7971.7, 7753.2, 8413.1, 8453.3, 9292.4, 9305.2, 9602.3, 9912.4])
  , ("Spinel sphere (diameter=3.6675mm)", [977.25, 1003.16, 1390.13, 1414.93, 1432.84, 1465.34, 1748.48, 1834.20, 1919.90, 1933.64, 1987.20, 2096.48, 2107.10, 2202.08, 2238.40, 2280.10, 0 {- 2290.53 calculated -}, 2400.88, 2435.85, 2507.80, 2546.30, 2608.55, 2652.35, 2691.70, 2708.00])
  ]

-- Local Variables:
-- truncate-lines:t
-- End:
