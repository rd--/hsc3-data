-- | Pdb types.  <https://www.wwpdb.org/documentation/file-format-content/format33/v3.3.html>
module Sound.Sc3.Data.Chemistry.Pdb.Types where

import Data.Function {- base -}
import Data.List {- base -}

import Music.Theory.Geometry.Matrix {- hmt-base -}
import Music.Theory.Geometry.Vector {- hmt-base -}

import qualified Music.Theory.List as T {- hmt -}

-- * Matrix

type Mtrx x = (M33 x, V3 x)

mtrx_identity :: Num n => Mtrx n
mtrx_identity = (((1, 0, 0), (0, 1, 0), (0, 0, 1)), (0, 0, 0))

mtrx_apply :: Num n => Mtrx n -> V3 n -> V3 n
mtrx_apply (m, v) x = v3_add (m33_apply m x) v

-- * Residue-Id

-- | (Name,Chain,Seqno,Inscode)
type Residue_Id = (String, Char, Int, Char)

residue_id_name :: Residue_Id -> String
residue_id_name (nm, _, _, _) = nm

residue_id_chain :: Residue_Id -> Char
residue_id_chain (_, ch, _, _) = ch

residue_id_seqno_inscode :: Residue_Id -> (Int, Char)
residue_id_seqno_inscode (_, _, k, i) = (k, i)

-- | Give (R1,R3) is R2 in range (inclusive).
residue_id_in_range :: (Residue_Id, Residue_Id) -> Residue_Id -> Bool
residue_id_in_range ((_, c1, k1, i1), (_, c3, k3, i3)) (_, c2, k2, i2) =
  if c1 /= c3 || (k1, i1) >= (k3, i3)
    then error "residue_id_in_range?"
    else (c2 == c1) && ((k1, i1) <= (k2, i2) && (k2, i2) <= (k3, i3))

-- * Record-Types

-- | (Hetatm,Serial,Name,Alt-Loc,Residue-Id,Coordinate,Element)
type Atom = (Bool, Int, String, Char, Residue_Id, (Double, Double, Double), String)

altloc_id_set :: [Char] -> [Char]
altloc_id_set x =
  case nub (sort x) of
    " " -> "NIL"
    ' ' : y -> y
    _ -> error "alt_id_set?"

atom_het :: Atom -> Bool
atom_het (h, _, _, _, _, _, _) = h

atom_serial :: Atom -> Int
atom_serial (_, k, _, _, _, _, _) = k

atom_name :: Atom -> String
atom_name (_, _, nm, _, _, _, _) = nm

atom_altloc :: Atom -> Char
atom_altloc (_, _, _, alt, _, _, _) = alt

atom_sel_altloc_A :: Atom -> Bool
atom_sel_altloc_A = flip elem " A" . atom_altloc

atom_residue_id :: Atom -> Residue_Id
atom_residue_id (_, _, _, _, r, _, _) = r

atom_chain_id :: Atom -> Char
atom_chain_id = residue_id_chain . atom_residue_id

atom_coord :: Atom -> (Double, Double, Double)
atom_coord (_, _, _, _, _, c, _) = c

atom_element :: Atom -> String
atom_element (_, _, _, _, _, _, e) = e

atom_element_or_name :: Atom -> String
atom_element_or_name (_, _, nm, _, _, _, el) = if null el then nm else el

type Conect = [(Int, Int)]

{- | ((A,B,C),(Alpha,Beta,Gamma),Space-Group,Z)

Non-Crystallography = ((1,1,1),(90,90,90),P1,1)
-}
type Cryst1 = ((Double, Double, Double), (Double, Double, Double), String, Int)

-- | (Classification,Dep-Date,Id-Code)
type Header = (String, String, String)

header_id4 :: Header -> String
header_id4 (_, _, x) = x

-- | ((Serial,Id),Init-Residue,End-Residue,Class,Length)
type Helix = ((Int, String), Residue_Id, Residue_Id, Int, Int)

helix_serial :: Helix -> Int
helix_serial ((k, _), _, _, _, _) = k

helix_chain_id :: Helix -> Char
helix_chain_id (_, (_, c1, _, _), (_, c2, _, _), _, _) = if c1 /= c2 then error "helix_chain_id?" else c1

type Het = (Residue_Id, Int, String)

het_id :: Het -> String
het_id ((x, _, _, _), _, _) = x

het_chain :: Het -> Char
het_chain ((_, x, _, _), _, _) = x

het_n_atom :: Het -> Int
het_n_atom (_, x, _) = x

-- | (Atom-Name-1,Alt-Loc-1,Residue-Id-1,Atom-Name-2,Alt-Loc-2,Residue-Id-2,Sym-1,Sym-2,Distance)
type Link = (String, Char, Residue_Id, String, Char, Residue_Id, Int, Int, Double)

-- | (Remark,Het,Helix,Sheet,Site,Xform,Coord,Ter,Conect,Seq)
type Master = (Int, Int, Int, Int, Int, Int, Int, Int, Int, Int)

-- | (Continuation,Text)
type MdlTyp = (String, String)

-- | (Id,Residue-Id,Std-Res,Comment)
type ModRes = (String, Residue_Id, String, String)

-- | (ResName,StdRes) fields of ModRes record.
modres_names :: ModRes -> (String, String)
modres_names (_, (r1, _, _, _), r2, _) = (r1, r2)

-- | (Type,Text)
type Remark = (Int, String)

-- | (Serial,Chain-Id,Num-Res,[Res])
type SeqRes = (Int, Char, Int, [String])

seqres_residue_names :: SeqRes -> [String]
seqres_residue_names (_, _, _, x) = x

-- | (Strand,Id,Num-Strands,Init-Residue,End-Residue)
type Sheet = (Int, String, Int, Residue_Id, Residue_Id)

sheet_chain_id :: Sheet -> Char
sheet_chain_id (_, _, _, (_, c1, _, _), (_, c2, _, _)) = if c1 /= c2 then error "sheet_chain_id?" else c1

-- | (Serial,Residue-Id-1,Residue-Id-2,Sym-1,Sym-2,Distance)
type SsBond = (Int, Residue_Id, Residue_Id, Int, Int, Double)

-- | (Serial,Residue-Id)
type Ter = (Int, Residue_Id)

-- | (Continuation,Title)
type Title = (String, String)

-- * Composite

-- | (Header,Title,NumMdl,Cryst1,(Atom,HetAtm),Conect,SeqRes,Helix,Sheet,Link,SsBond)
type Pdb =
  ( Header
  , String
  , Maybe Int
  , Cryst1
  , ([Atom], [Atom])
  , [(Int, Int)]
  , [(Char, [String])]
  , [Helix]
  , [Sheet]
  , [Link]
  , [SsBond]
  )

-- * Group

-- | Group atoms by chain and ensure sequence
atom_group :: [Atom] -> [(Char, [Atom])]
atom_group = map (fmap (sortOn atom_serial)) . T.collate_on atom_chain_id id

-- | Merge Conect records, sort and remove (i,j)-(j,i) duplicates.
conect_group :: [Conect] -> [(Int, Int)]
conect_group =
  let o (i, j) = (min i j, max i j)
  in nub . sort . map o . concat

-- | Group helices by chain and ensure sequence
helix_group :: [Helix] -> [(Char, [Helix])]
helix_group = map (fmap (sortOn helix_serial)) . T.collate_on helix_chain_id id

mdltyp_group :: [MdlTyp] -> String
mdltyp_group = unwords . map snd . sort

-- | Group residues by Chain.
seqres_group :: [SeqRes] -> [(Char, [String])]
seqres_group =
  let f (k, c, _, _) = (c, k)
      g (_, c, _, _) = c
      h (_, c, _, r) = (c, r)
      i j = let (c, r) = unzip j in (head c, concat r)
  in map (i . map h) . groupBy ((==) `on` g) . sortOn f

-- | Group helices by chain
sheet_group :: [Sheet] -> [(Char, [Sheet])]
sheet_group = T.collate_on sheet_chain_id id

title_group :: [Title] -> String
title_group = unwords . map snd . sort

-- * Ter

-- | Remove 'Atom's that are located past any 'Ter' entry for the chain.
atom_apply_ter :: [Ter] -> (Char, [Atom]) -> (Char, [Atom])
atom_apply_ter ter (ch, a) =
  case find (\(_, (_, x, _, _)) -> x == ch) ter of
    Just (k, _) -> (ch, filter ((< k) . atom_serial) a)
    _ -> (ch, a)
