{- | Periodic table of the elements.
  <https://pubchem.ncbi.nlm.nih.gov/periodic-table/>
-}
module Sound.Sc3.Data.Chemistry.Elements where

import Data.Char {- base -}
import Data.Either {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}

import qualified Music.Theory.List as List {- hmt-base -}

import Music.Theory.Geometry.Vector {- hmt-base -}

{- | The angstrom or ångström is a metric unit of length equal to 1×10−10 m,
or one ten-billionth of a metre, 0.1 nanometre, or 100 picometres.
Its symbol is Å.

The picometre (SI symbol: pm) is a metric unit of length equal to 1×10−12 m,
or one trillionth of a metre.
-}
data E_Length_Unit = Angstroms | Picometres

-- | 100 pm = 1 Å
picometres_to_angstroms :: Fractional n => n -> n
picometres_to_angstroms = (/ 100)

-- | 1 Å = 100 pm
angstroms_to_picometres :: Num n => n -> n
angstroms_to_picometres = (* 100)

-- | Atomic number (1 - 118)
type Atomic_Number = Int

-- | Atomic symbol (1-2 char)
type Atomic_Symbol = String

{- | The dalton (Da) or unified atomic mass unit (u) is defined as
1/12 of the mass of an unbound neutral atom of carbon-12 in its
nuclear and electronic ground state and at rest.
-}
type Dalton = Double

{- | (atomic-number,atomic-symbol,name,standard-atomic-weight)

>>> let f (_,sym,_,_) = if length sym == 1 then Just (List.head_err sym) else Nothing
>>> Data.List.sort (mapMaybe f periodic_table)
"BCFHIKNOPSUVWY"

>>> Data.List.nub (map (\(_,sym,_,_) -> length sym) periodic_table)
[1,2]

Commission on Isotopic Abundances and Atomic Weights,
The International Union of Pure and Applied Chemistry,
<https://ciaaw.org/atomic-weights.htm>

<https://www.qmul.ac.uk/sbcs/iupac/AtWt/>
-}
periodic_table :: [(Atomic_Number, Atomic_Symbol, String, Dalton)]
periodic_table =
  [ (1, "H", "Hydrogen", 1.00794)
  , (2, "He", "Helium", 4.002602)
  , (3, "Li", "Lithium", 6.941)
  , (4, "Be", "Beryllium", 9.012182)
  , (5, "B", "Boron", 10.811)
  , (6, "C", "Carbon", 12.0107)
  , (7, "N", "Nitrogen", 14.0067)
  , (8, "O", "Oxygen", 15.9994)
  , (9, "F", "Fluorine", 18.9984032)
  , (10, "Ne", "Neon", 20.1797)
  , (11, "Na", "Sodium", 22.98976928)
  , (12, "Mg", "Magnesium", 24.3050)
  , (13, "Al", "Aluminum", 26.9815386)
  , (14, "Si", "Silicon", 28.0855)
  , (15, "P", "Phosphorus", 30.973762)
  , (16, "S", "Sulfur", 32.065)
  , (17, "Cl", "Chlorine", 35.453)
  , (18, "Ar", "Argon", 39.948)
  , (19, "K", "Potassium", 39.0983)
  , (20, "Ca", "Calcium", 40.078)
  , (21, "Sc", "Scandium", 44.955912)
  , (22, "Ti", "Titanium", 47.867)
  , (23, "V", "Vanadium", 50.9415)
  , (24, "Cr", "Chromium", 51.9961)
  , (25, "Mn", "Manganese", 54.938045)
  , (26, "Fe", "Iron", 55.845)
  , (27, "Co", "Cobalt", 58.933195)
  , (28, "Ni", "Nickel", 58.6934)
  , (29, "Cu", "Copper", 63.546)
  , (30, "Zn", "Zinc", 65.38)
  , (31, "Ga", "Gallium", 69.723)
  , (32, "Ge", "Germanium", 72.64)
  , (33, "As", "Arsenic", 74.92160)
  , (34, "Se", "Selenium", 78.96)
  , (35, "Br", "Bromine", 79.904)
  , (36, "Kr", "Krypton", 83.798)
  , (37, "Rb", "Rubidium", 85.4678)
  , (38, "Sr", "Strontium", 87.62)
  , (39, "Y", "Yttrium", 88.90585)
  , (40, "Zr", "Zirconium", 91.224)
  , (41, "Nb", "Niobium", 92.90638)
  , (42, "Mo", "Molybdenum", 95.96)
  , (43, "Tc", "Technetium", 98)
  , (44, "Ru", "Ruthenium", 101.07)
  , (45, "Rh", "Rhodium", 102.90550)
  , (46, "Pd", "Palladium", 106.42)
  , (47, "Ag", "Silver", 107.8682)
  , (48, "Cd", "Cadmium", 112.411)
  , (49, "In", "Indium", 114.818)
  , (50, "Sn", "Tin", 118.710)
  , (51, "Sb", "Antimony", 121.760)
  , (52, "Te", "Tellurium", 127.60)
  , (53, "I", "Iodine", 126.90447)
  , (54, "Xe", "Xenon", 131.293)
  , (55, "Cs", "Cesium", 132.9054519)
  , (56, "Ba", "Barium", 137.327)
  , (57, "La", "Lanthanum", 138.90547)
  , (58, "Ce", "Cerium", 140.116)
  , (59, "Pr", "Praseodymium", 140.90765)
  , (60, "Nd", "Neodymium", 144.242)
  , (61, "Pm", "Promethium", 145)
  , (62, "Sm", "Samarium", 150.36)
  , (63, "Eu", "Europium", 151.964)
  , (64, "Gd", "Gadolinium", 157.25)
  , (65, "Tb", "Terbium", 158.92535)
  , (66, "Dy", "Dysprosium", 162.500)
  , (67, "Ho", "Holmium", 164.93032)
  , (68, "Er", "Erbium", 167.259)
  , (69, "Tm", "Thulium", 168.93421)
  , (70, "Yb", "Ytterbium", 173.054)
  , (71, "Lu", "Lutetium", 174.9668)
  , (72, "Hf", "Hafnium", 178.49)
  , (73, "Ta", "Tantalum", 180.94788)
  , (74, "W", "Tungsten", 183.84)
  , (75, "Re", "Rhenium", 186.207)
  , (76, "Os", "Osmium", 190.23)
  , (77, "Ir", "Iridium", 192.217)
  , (78, "Pt", "Platinum", 195.084)
  , (79, "Au", "Gold", 196.966569)
  , (80, "Hg", "Mercury", 200.59)
  , (81, "Tl", "Thallium", 204.3833)
  , (82, "Pb", "Lead", 207.2)
  , (83, "Bi", "Bismuth", 208.98040)
  , (84, "Po", "Polonium", 209)
  , (85, "At", "Astatine", 210)
  , (86, "Rn", "Radon", 222)
  , (87, "Fr", "Francium", 223)
  , (88, "Ra", "Radium", 226)
  , (89, "Ac", "Actinium", 227)
  , (90, "Th", "Thorium", 232.03806)
  , (91, "Pa", "Protactinium", 231.03588)
  , (92, "U", "Uranium", 238.02891)
  , (93, "Np", "Neptunium", 237)
  , (94, "Pu", "Plutonium", 244)
  , (95, "Am", "Americium", 243)
  , (96, "Cm", "Curium", 247)
  , (97, "Bk", "Berkelium", 247)
  , (98, "Cf", "Californium", 251)
  , (99, "Es", "Einsteinium", 252)
  , (100, "Fm", "Fermium", 257)
  , (101, "Md", "Mendelevium", 258)
  , (102, "No", "Nobelium", 259)
  , (103, "Lr", "Lawrencium", 262)
  , (104, "Rf", "Rutherfordium", 267)
  , (105, "Db", "Dubnium", 268)
  , (106, "Sg", "Seaborgium", 271)
  , (107, "Bh", "Bohrium", 272)
  , (108, "Hs", "Hassium", 270)
  , (109, "Mt", "Meitnerium", 276)
  , (110, "Ds", "Darmstadtium", 281)
  , (111, "Rg", "Roentgenium", 280)
  , (112, "Cn", "Copernicium", 285)
  , (113, "Nh", "Nihonium", 284)
  , (114, "Fl", "Flerovium", 289)
  , (115, "Mc", "Moscovium", 288)
  , (116, "Lv", "Livermorium", 293)
  , (117, "Ts", "Tennessine", 294)
  , (118, "Og", "Oganesson", 294)
  ]

{- | Lookup atomic symbol in 'periodic_table' and return atomic number.
If /cs/ is False then match case-insensitively.

>>> mapMaybe (atomic_number True) (map return ['A' .. 'Z'])
[5,6,9,1,53,19,7,8,15,16,92,23,74,39]
-}
atomic_number :: Bool -> Atomic_Symbol -> Maybe Atomic_Number
atomic_number cs x =
  let u = if cs then id else map toUpper
  in lookup (u x) (map (\(k, sym, _, _) -> (u sym, k)) periodic_table)

{- | Erroring variant.

>>> map (atomic_number_err True) (words "C Sc Ag")
[6,21,47]

>>> map (atomic_number_err True) (map return "BCFHIKNOPSUVWY")
[5,6,9,1,53,19,7,8,15,16,92,23,74,39]

>>> atomic_number_err False "FE"
26
-}
atomic_number_err :: Bool -> Atomic_Symbol -> Atomic_Number
atomic_number_err cs sym = fromMaybe (error ("atomic_number: " ++ sym)) (atomic_number cs sym)

-- | Lookup atomic number in 'periodic_table' and return atomic weight.
atomic_weight :: Atomic_Number -> Maybe Dalton
atomic_weight x = lookup x (map (\(k, _, _, w) -> (k, w)) periodic_table)

-- | Erroring variant.
atomic_weight_err :: Atomic_Number -> Dalton
atomic_weight_err x = fromMaybe (error ("atomic_weight: " ++ show x)) (atomic_weight x)

{- | (atomic-number,covalent-radius:picometres)

Cordero et al. (2008), and Pyykkö and Atsumi (2009)

>>> let r = map snd covalent_radii_table
>>> (minimum r,Music.Theory.Math.arithmetic_mean_of_list r,maximum r)
(28,152.0,260)
-}
covalent_radii_table :: Num n => [(Atomic_Number, n)]
covalent_radii_table =
  [ (001, 31)
  , (002, 28)
  , (003, 128)
  , (004, 96)
  , (005, 85)
  , (006, 76)
  , (007, 71)
  , (008, 66)
  , (009, 57)
  , (010, 58)
  , (011, 166)
  , (012, 141)
  , (013, 121)
  , (014, 111)
  , (015, 107)
  , (016, 105)
  , (017, 102)
  , (018, 106)
  , (019, 203)
  , (020, 176)
  , (021, 170)
  , (022, 160)
  , (023, 153)
  , (024, 139)
  , (025, 139)
  , (026, 132)
  , (027, 126)
  , (028, 124)
  , (029, 132)
  , (030, 122)
  , (031, 122)
  , (032, 120)
  , (033, 119)
  , (034, 120)
  , (035, 120)
  , (036, 116)
  , (037, 220)
  , (038, 195)
  , (039, 190)
  , (040, 175)
  , (041, 164)
  , (042, 154)
  , (043, 147)
  , (044, 146)
  , (045, 142)
  , (046, 139)
  , (047, 145)
  , (048, 144)
  , (049, 142)
  , (050, 139)
  , (051, 139)
  , (052, 138)
  , (053, 139)
  , (054, 140)
  , (055, 244)
  , (056, 215)
  , (057, 207)
  , (058, 204)
  , (059, 203)
  , (060, 201)
  , (061, 199)
  , (062, 198)
  , (063, 198)
  , (064, 196)
  , (065, 194)
  , (066, 192)
  , (067, 192)
  , (068, 189)
  , (069, 190)
  , (070, 187)
  , (071, 187)
  , (072, 175)
  , (073, 170)
  , (074, 162)
  , (075, 151)
  , (076, 144)
  , (077, 141)
  , (078, 136)
  , (079, 136)
  , (080, 132)
  , (081, 145)
  , (082, 146)
  , (083, 148)
  , (084, 140)
  , (085, 150)
  , (086, 150)
  , (087, 260)
  , (088, 221)
  , (089, 215)
  , (090, 206)
  , (091, 200)
  , (092, 196)
  , (093, 190)
  , (094, 187)
  , (095, 180)
  , (096, 169)
  ]

-- | Lookup covalent radius of element (given by atomic number) in 'covalent_radii_table'.
covalent_radius :: Num n => Atomic_Number -> Maybe n
covalent_radius k = lookup k covalent_radii_table

-- | Erroring variant.
covalent_radius_err :: Num n => Atomic_Number -> n
covalent_radius_err = fromMaybe (error "covalent_radii") . covalent_radius

{- | (atomic-number,covalent-radius:angstroms)

<https://www.ccdc.cam.ac.uk/support-and-resources/ccdcresources/Elemental_Radii.xlsx>

>>> let round_f = fromIntegral . round
>>> let r = map (round_f . angstroms_to_picometres. snd) covalent_radii_csd_table
>>> (minimum r,round (Music.Theory.Math.arithmetic_mean_of_list r),maximum r)
(23,155,260)
-}
covalent_radii_csd_table :: Fractional n => [(Atomic_Number, n)]
covalent_radii_csd_table =
  [ (001, 0.23)
  , (002, 1.5)
  , (003, 1.28)
  , (004, 0.96)
  , (005, 0.83)
  , (006, 0.68)
  , (007, 0.68)
  , (008, 0.68)
  , (009, 0.64)
  , (010, 1.5)
  , (011, 1.66)
  , (012, 1.41)
  , (013, 1.21)
  , (014, 1.2)
  , (015, 1.05)
  , (016, 1.02)
  , (017, 0.99)
  , (018, 1.51)
  , (019, 2.03)
  , (020, 1.76)
  , (021, 1.7)
  , (022, 1.6)
  , (023, 1.53)
  , (024, 1.39)
  , (025, 1.61)
  , (026, 1.52)
  , (027, 1.26)
  , (028, 1.24)
  , (029, 1.32)
  , (030, 1.22)
  , (031, 1.22)
  , (032, 1.17)
  , (033, 1.21)
  , (034, 1.22)
  , (035, 1.21)
  , (036, 1.5)
  , (037, 2.2)
  , (038, 1.95)
  , (039, 1.9)
  , (040, 1.75)
  , (041, 1.64)
  , (042, 1.54)
  , (043, 1.47)
  , (044, 1.46)
  , (045, 1.42)
  , (046, 1.39)
  , (047, 1.45)
  , (048, 1.54)
  , (049, 1.42)
  , (050, 1.39)
  , (051, 1.39)
  , (052, 1.47)
  , (053, 1.4)
  , (054, 1.5)
  , (055, 2.44)
  , (056, 2.15)
  , (057, 2.07)
  , (058, 2.04)
  , (059, 2.03)
  , (060, 2.01)
  , (061, 1.99)
  , (062, 1.98)
  , (063, 1.98)
  , (064, 1.96)
  , (065, 1.94)
  , (066, 1.92)
  , (067, 1.92)
  , (068, 1.89)
  , (069, 1.9)
  , (070, 1.87)
  , (071, 1.87)
  , (072, 1.75)
  , (073, 1.7)
  , (074, 1.62)
  , (075, 1.51)
  , (076, 1.44)
  , (077, 1.41)
  , (078, 1.36)
  , (079, 1.36)
  , (080, 1.32)
  , (081, 1.45)
  , (082, 1.46)
  , (083, 1.48)
  , (084, 1.4)
  , (085, 1.21)
  , (086, 1.5)
  , (087, 2.6)
  , (088, 2.21)
  , (089, 2.15)
  , (090, 2.06)
  , (091, 2)
  , (092, 1.96)
  , (093, 1.9)
  , (094, 1.87)
  , (095, 1.8)
  , (096, 1.69)
  , (097, 1.54)
  , (098, 1.83)
  , (099, 1.5)
  , (100, 1.5)
  , (101, 1.5)
  , (102, 1.5)
  , (103, 1.5)
  , (104, 1.5)
  , (105, 1.5)
  , (106, 1.5)
  , (107, 1.5)
  , (108, 1.5)
  , (109, 1.5)
  , (110, 1.5)
  ]

-- | Lookup covalent radius of element (given by atomic number) in 'covalent_radii_csd_table'.
covalent_radius_csd :: Fractional n => Atomic_Number -> Maybe n
covalent_radius_csd k = lookup k covalent_radii_csd_table

-- | Erroring variant.
covalent_radius_csd_err :: Fractional n => Atomic_Number -> n
covalent_radius_csd_err = fromMaybe (error "covalent_radii_csd") . covalent_radius_csd

type Dist_Msr t = (((Int, Int), (V3 t, V3 t)), t, t, t, Bool, Bool)

{- | /rad_f/ is the atomic symbol to covalent radius function.
/(t_min,t_max)/ are the tolerance values, /t_min/ is ordinarily negative.
-}
calculate_distance_set :: (Ord t, Floating t) => (Atomic_Symbol -> t) -> (t, t) -> [(Atomic_Symbol, V3 t)] -> [Dist_Msr t]
calculate_distance_set rad_f (t_min, t_max) e =
  let f (k0, (a0, p0)) (k1, (a1, p1)) =
        if k0 < k1
          then
            let r0 = rad_f a0
                r1 = rad_f a1
                r2 = r0 + r1
                d = v3_distance p0 p1
            in Just
                ( ((k0, k1), (p0, p1))
                , r1
                , r2
                , d
                , d > (r2 + t_min)
                , d < (r2 + t_max)
                )
          else Nothing
      e_ix = zip [0 ..] e
  in catMaybes [f i j | i <- e_ix, j <- e_ix]

{- | Given a radius function (atomic-symbol -> covalent radius)
calculate bonds for a set of atoms.

The rule is: if the distance between two atoms is within a
tolerance of the sum of their covalent radii, they are considered
bonded.
-}
calculate_bonds :: (Floating n, Ord n) => (Atomic_Symbol -> n) -> (n, n) -> [(Atomic_Symbol, V3 n)] -> [((Int, Int), (V3 n, V3 n))]
calculate_bonds rad_f th =
  let f (r, _, _, _, t0, t1) = if t0 && t1 then Just r else Nothing
  in mapMaybe f . calculate_distance_set rad_f th

-- | (atomic-number,cpk-colour:rgb24)
cpk_color_table :: Num n => [(Atomic_Number, n)]
cpk_color_table =
  [ (001, 0xFFFFFF)
  , (002, 0xD9FFFF)
  , (003, 0xCC80FF)
  , (004, 0xC2FF00)
  , (005, 0xFFB5B5)
  , (006, 0x909090)
  , (007, 0x3050F8)
  , (008, 0xFF0D0D)
  , (009, 0x90E050)
  , (010, 0xB3E3F5)
  , (011, 0xAB5CF2)
  , (012, 0x8AFF00)
  , (013, 0xBFA6A6)
  , (014, 0xF0C8A0)
  , (015, 0xFF8000)
  , (016, 0xFFFF30)
  , (017, 0x1FF01F)
  , (018, 0x80D1E3)
  , (019, 0x8F40D4)
  , (020, 0x3DFF00)
  , (021, 0xE6E6E6)
  , (022, 0xBFC2C7)
  , (023, 0xA6A6AB)
  , (024, 0x8A99C7)
  , (025, 0x9C7AC7)
  , (026, 0xE06633)
  , (027, 0xF090A0)
  , (028, 0x50D050)
  , (029, 0xC88033)
  , (030, 0x7D80B0)
  , (031, 0xC28F8F)
  , (032, 0x668F8F)
  , (033, 0xBD80E3)
  , (034, 0xFFA100)
  , (035, 0xA62929)
  , (036, 0x5CB8D1)
  , (037, 0x702EB0)
  , (038, 0x00FF00)
  , (039, 0x94FFFF)
  , (040, 0x94E0E0)
  , (041, 0x73C2C9)
  , (042, 0x54B5B5)
  , (043, 0x3B9E9E)
  , (044, 0x248F8F)
  , (045, 0x0A7D8C)
  , (046, 0x006985)
  , (047, 0xC0C0C0)
  , (048, 0xFFD98F)
  , (049, 0xA67573)
  , (050, 0x668080)
  , (051, 0x9E63B5)
  , (052, 0xD47A00)
  , (053, 0x940094)
  , (054, 0x429EB0)
  , (055, 0x57178F)
  , (056, 0x00C900)
  , (057, 0x70D4FF)
  , (058, 0xFFFFC7)
  , (059, 0xD9FFC7)
  , (060, 0xC7FFC7)
  , (061, 0xA3FFC7)
  , (062, 0x8FFFC7)
  , (063, 0x61FFC7)
  , (064, 0x45FFC7)
  , (065, 0x30FFC7)
  , (066, 0x1FFFC7)
  , (067, 0x00FF9C)
  , (068, 0x000000)
  , (069, 0x00D452)
  , (070, 0x00BF38)
  , (071, 0x00AB24)
  , (072, 0x4DC2FF)
  , (073, 0x4DA6FF)
  , (074, 0x2194D6)
  , (075, 0x267DAB)
  , (076, 0x266696)
  , (077, 0x175487)
  , (078, 0xD0D0E0)
  , (079, 0xFFD123)
  , (080, 0xB8B8D0)
  , (081, 0xA6544D)
  , (082, 0x575961)
  , (083, 0x9E4FB5)
  , (084, 0xAB5C00)
  , (085, 0x754F45)
  , (086, 0x428296)
  , (087, 0x420066)
  , (088, 0x007D00)
  , (089, 0x70ABFA)
  , (090, 0x00BAFF)
  , (091, 0x00A1FF)
  , (092, 0x008FFF)
  , (093, 0x0080FF)
  , (094, 0x006BFF)
  , (095, 0x545CF2)
  , (096, 0x785CE3)
  , (097, 0x8A4FE3)
  , (098, 0xA136D4)
  , (099, 0xB31FD4)
  , (100, 0xB31FBA)
  , (101, 0xB30DA6)
  , (102, 0xBD0D87)
  , (103, 0xC70066)
  , (104, 0xCC0059)
  , (105, 0xD1004F)
  , (106, 0xD90045)
  , (107, 0xE00038)
  , (108, 0xE6002E)
  , (109, 0xEB0026)
  ]

-- | Lookup 'cpk_color_table'.
cpk_color :: Num n => Atomic_Number -> Maybe n
cpk_color k = lookup k cpk_color_table

-- | Erroring variant.
cpk_color_err :: Num n => Atomic_Number -> n
cpk_color_err = fromMaybe (error "cpk_color") . cpk_color

-- | (atomic-number,atomic-radius,ion-radius,van-del-waals-radius)
atomic_ion_vdw_radii_table :: Fractional n => [(Atomic_Number, n, n, n)]
atomic_ion_vdw_radii_table =
  [ (1, 37, -1, 120)
  , (2, 32, -1, 140)
  , (3, 134, 76, 182)
  , (4, 90, 45, -1)
  , (5, 82, 27, -1)
  , (6, 77, 16, 170)
  , (7, 75, 146, 155)
  , (8, 73, 140, 152)
  , (9, 71, 133, 147)
  , (10, 69, -1, 154)
  , (11, 154, 102, 227)
  , (12, 130, 72, 173)
  , (13, 118, 53.5, -1)
  , (14, 111, 40, 210)
  , (15, 106, 44, 180)
  , (16, 102, 184, 180)
  , (17, 99, 181, 175)
  , (18, 97, -1, 188)
  , (19, 196, 138, 275)
  , (20, 174, 100, -1)
  , (21, 144, 74.5, -1)
  , (22, 136, 86, -1)
  , (23, 125, 79, -1)
  , (24, 127, 80, -1)
  , (25, 139, 67, -1)
  , (26, 125, 78, -1)
  , (27, 126, 74.5, -1)
  , (28, 121, 69, 163)
  , (29, 138, 77, 140)
  , (30, 131, 74, 139)
  , (31, 126, 62, 187)
  , (32, 122, 73, -1)
  , (33, 119, 58, 185)
  , (34, 116, 198, 190)
  , (35, 114, 196, 185)
  , (36, 110, -1, 202)
  , (37, 211, 152, -1)
  , (38, 192, 118, -1)
  , (39, 162, 90, -1)
  , (40, 148, 72, -1)
  , (41, 137, 72, -1)
  , (42, 145, 69, -1)
  , (43, 156, 64.5, -1)
  , (44, 126, 68, -1)
  , (45, 135, 66.5, -1)
  , (46, 131, 59, 163)
  , (47, 153, 115, 172)
  , (48, 148, 95, 158)
  , (49, 144, 80, 193)
  , (50, 141, 112, 217)
  , (51, 138, 76, -1)
  , (52, 135, 221, 206)
  , (53, 133, 220, 198)
  , (54, 130, 48, 216)
  , (55, 225, 167, -1)
  , (56, 198, 135, -1)
  , (57, 169, 103.2, -1)
  , (58, -1, 102, -1)
  , (59, -1, 99, -1)
  , (60, -1, 129, -1)
  , (61, -1, 97, -1)
  , (62, -1, 122, -1)
  , (63, -1, 117, -1)
  , (64, -1, 93.8, -1)
  , (65, -1, 92.3, -1)
  , (66, -1, 107, -1)
  , (67, -1, 90.1, -1)
  , (68, -1, 89, -1)
  , (69, -1, 103, -1)
  , (70, -1, 102, -1)
  , (71, 160, 86.1, -1)
  , (72, 150, 71, -1)
  , (73, 138, 72, -1)
  , (74, 146, 66, -1)
  , (75, 159, 63, -1)
  , (76, 128, 63, -1)
  , (77, 137, 68, -1)
  , (78, 128, 86, 175)
  , (79, 144, 137, 166)
  , (80, 149, 119, 155)
  , (81, 148, 150, 196)
  , (82, 147, 119, 202)
  , (83, 146, 103, -1)
  , (84, -1, 94, -1)
  , (85, -1, 62, -1)
  , (86, 145, -1, -1)
  , (87, -1, 180, -1)
  , (88, -1, 148, -1)
  , (89, -1, 112, -1)
  , (90, -1, 94, -1)
  , (91, -1, 104, -1)
  , (92, -1, 102.5, 186)
  , (93, -1, 110, -1)
  , (94, -1, 100, -1)
  , (95, -1, 126, -1)
  , (96, -1, 97, -1)
  , (97, -1, 96, -1)
  , (98, -1, 95, 245) -- https://periodic.lanl.gov/98.shtml
  , (99, -1, -1, -1)
  , (100, -1, -1, -1)
  , (101, -1, -1, -1)
  , (102, -1, -1, -1)
  , (103, -1, -1, -1)
  , (104, -1, -1, -1)
  , (105, -1, -1, -1)
  , (106, -1, -1, -1)
  , (107, -1, -1, -1)
  , (108, -1, -1, -1)
  , (109, -1, -1, -1)
  , (110, -1, -1, -1)
  , (111, -1, -1, -1)
  , (112, -1, -1, -1)
  , (113, -1, -1, -1)
  , (114, -1, -1, -1)
  , (115, -1, -1, -1)
  , (116, -1, -1, -1)
  , (117, -1, -1, -1)
  , (118, -1, -1, -1)
  ]

-- * Formula

{- | Given real signum, ie. -1, 0 or +1, given character of sign.

>>> map (signum_to_char . signum) [-10,0,10]
[Just '-',Nothing,Just '+']
-}
signum_to_char :: (Eq a, Num a) => a -> Maybe Char
signum_to_char x =
  case x of
    -1 -> Just '-'
    0 -> Nothing
    1 -> Just '+'
    _ -> error "signum_to_char?"

-- | [(Element-Symbol,Count)]
type Formula = [(String, Int)]

-- | Ordered Formula and Charge.
type Formula_Ch = (Formula, Maybe Int)

-- | Format Formula.
formula_pp :: Formula -> String
formula_pp =
  let f (sym, k) = if k == 1 then sym else sym ++ show k
  in unwords . map f

-- | Format Formula_Ch.
formula_ch_pp :: Formula_Ch -> String
formula_ch_pp (e, c) =
  let c' = maybe "" (\x -> show (abs x) ++ maybe "" return (signum_to_char x)) c
  in formula_pp e ++ c'

{- | Inverse of 'formula_ch_pp', histogram is un-sorted.

>>> map (formula_ch_pp . formula_ch_parse) ["H2 O","C2 H4 O3","O4 S1"]
["H2 O","C2 H4 O3","O4 S"]

>>> map formula_ch_parse ["H A","C21 H26 Cl1 N4 O2 1+","O4 S1 2-"]
[([("H",1),("A",1)],Nothing),([("C",21),("H",26),("Cl",1),("N",4),("O",2)],Just 1),([("O",4),("S",1)],Just (-2))]
-}
formula_ch_parse :: String -> Formula_Ch
formula_ch_parse txt =
  let f x = case span isDigit x of
        ([], _) -> case span isAlpha x of
          ("", _) -> error ("formula_ch_parse: " ++ x)
          (e, "") -> Left (e, 1)
          (e, n) -> Left (e, read n)
        (k, ['-']) -> Right (negate (read k))
        (k, ['+']) -> Right (read k)
        _ -> error "formula_ch_parse?"
  in case partitionEithers (map f (words txt)) of
      (e, []) -> (e, Nothing)
      (e, [c]) -> (e, Just c)
      _ -> error ("formula_ch_parse: " ++ txt)

-- | Parse Formula, it is an 'error' for Charge to be given.
formula_parse :: String -> Formula
formula_parse txt =
  case formula_ch_parse txt of
    (e, Nothing) -> e
    _ -> error ("formula_parse: " ++ txt)

{- | Hill formula sequence.

The elements of the chemical formula are given in Hill ordering.
The order of elements depends on whether carbon is present or not.
If carbon is present, the order should be: C, then H, then the other elements in alphabetical order.
If carbon is not present, the elements are listed purely in alphabetic order of their symbol.
This is the 'Hill' system used by Chemical Abstracts.
-}
formula_sort_hill :: Formula -> Formula
formula_sort_hill e =
  case lookup "C" e of
    Nothing -> e
    Just _ -> let (p, q) = partition (flip elem ["C", "H"] . fst) e in p ++ q

{- | 'formula_sort_hill' of 'List.histogram'.

>>> map (formula_pp . hill_formula_seq . words) ["A C H","A H","A C"]
["C H A","A H","C A"]
-}
hill_formula_seq :: [String] -> Formula
hill_formula_seq = formula_sort_hill . List.histogram

-- | 'formula_pp' of 'hill_formula_seq'
hill_formula :: [String] -> String
hill_formula = formula_pp . hill_formula_seq

-- | Apply 'abs' to count at Formula.
formula_abs :: Formula -> Formula
formula_abs = map (fmap abs)

{- | Difference between two formula.

>>> let f f1 f2 = formula_pp (formula_diff (formula_parse f1) (formula_parse f2))
>>> f "C4 H9 N1 O3" "C4 H7 N1 O2"
"H2 O"

>>> f "H2 O" "C4 H O"
"H C4"
-}
formula_diff :: Formula -> Formula -> Formula
formula_diff x y =
  let f (e, m) = case lookup e y of
        Nothing -> Just (e, m)
        Just n -> if n == m then Nothing else Just (e, m - n)
      g (e1, _) (e2, _) = e1 == e2
  in mapMaybe f x ++ deleteFirstsBy g y x
