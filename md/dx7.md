# dx7

## sysex add

Add a DX7 sysex containter to a 4096 byte-sequence DX7 packed voice-bank data chunk.

## sysex print

~~~~
$ cd ~/sw/hsc3-data/data/yamaha/dx7/rom/
$ hsc3-dx7 sysex print voice-names DX7-ROM1A.syx
BRASS   1
BRASS   2
BRASS   3
STRINGS 1
STRINGS 2
STRINGS 3
...
$ hsc3-dx7 sysex print parameters DX7-ROM1A.syx
000: OP 6 EG RATE 1 = 49
001: OP 6 EG RATE 2 = 99
002: OP 6 EG RATE 3 = 28
003: OP 6 EG RATE 4 = 68
004: OP 6 EG LEVEL 1 = 98
005: OP 6 EG LEVEL 2 = 98
006: OP 6 EG LEVEL 3 = 91
007: OP 6 EG LEVEL 4 = 0
...
$ hsc3-dx7 sysex print voice-data-list DX7-ROM1A.syx
NAME=BRASS   1
ALGORITHM=22
FEEDBACK=07
LFO
  WAVE=SI
  SPEED=37
  DELAY=00
  PMD=05
  AMD=00
  SYNC=OFF
MOD SENSITIVITY
  PITCH=03
  AMPLITUDE=00,00,00,00,00,00
OSCILLATOR
  MOD=RATIO,RATIO,RATIO,RATIO,RATIO,RATIO
  SYNC=ON
  FREQ. COARSE=01,01,01,01,00,00
  FREQ. FINE=00,00,00,00,00,00
  DETUNE=00,01,00,-2,07,07
EG
  R1=49,77,77,77,62,72
  R2=99,36,36,76,51,76
  R3=28,41,41,82,29,99
  R4=68,71,71,71,71,71
  L1=98,99,99,99,82,99
  L2=98,98,98,98,95,88
  L3=91,98,98,98,96,96
  L4=00,00,00,00,00,00
...
$
~~~~

## sysex rewrite

This command ignores the existing SYSEX container, which may contain
errors, and makes a new SYSEX container for the packed data sequence.

## sysex verify

~~~~
$ hsc3-dx7 sysex verify DX7-ROM1A.syx
TRUE
$
~~~~
