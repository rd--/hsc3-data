# sf-draw

Generate `PBM` drawing of a `NeXT/AU` sound file.

There are options useful for making drawings of (0,1) data tables stored in audio files.
