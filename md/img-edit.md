# img-edit

Image editing operations.

## pbm le {r|l|d|u} input-file output-file

Leading edges transform in indicated direction, r = right, l = left, d = down, u = up.

## pbm le/all pbm-file

All leading edge transforms, file names are derived automatically, `.le.r.pbm` etc.
