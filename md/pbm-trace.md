# pbm-trace

Trace connected paths in a `PBM` file.

Paths are written in three forms:

- each trace to a separate `PBM` file
- a composite of all traces to a `PBM` file
- a `CSV` file where the columns are (trace-index,row-index,column-index).
